#! /usr/bin/python -O
# -*- coding: utf-8 -*-
# kaspar_primers_triplets_design_v_0_2.py
# creation:      cv - 27-03-2014
# version 0.2 : jpb - 08-07-2014
# adding new parameters : -folder, -max_primer, -max_triplet, -max_N_global, -max_N_local, -max_Ambi_global, -max_Ambi_local
# partial rewriting of most functions
# complete rewriting of the parts assessing the specificies of the triplets for a given reference genome

__version__ = "0.2"

# LICENSE
# ------------------------------------------------------------------------
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# 
# Authors: INRA - GAFL - UR 1052 Génétique et Amélioration des Fruits et Légumes
# Domaine Saint-Maurice - CS 60094 - F-84143 Montfavet cedex - France
# Jean-Paul Bouchet - jean-paul.bouchet@ainra.fr
# Jehan-Baptiste Mauroux
# Christophe Vouland
# 

"""kaspar_primers_triplets_design_v_0_2.py
This program designs one or several triplets of primers from a list of SNPs
"""

# imported modules
# import commands, os, string, sys, csv, shutil
import commands, os, string, sys, csv, shutil

# list of functions
# adding_specificities_informations()
# chop(s)
# error(no, s = "")
# files_close()
# help()
# help_detailed()
# getting_default_pcr_parameters()
# primer3_parser(fl_primer3_output_name, fl_primer3_output_parser_name, direction)
# primers_triplet_specificity(i, j, direction)
# primers_triplets_design()
# primers_triplets_file_writing(fl_fam_primer3_output_parser_name, fl_hex_primer3_output_parser_name)
# print_msg(msg = "\n")
# testing_primers_triplets_specificities()
# testing_sequence(seq, seq_id, snp_nb)
# writing_all_primers_triplets_files()
# writing_primer3_input_files()
# writing_primers_triplets_specificities_file()



#=========================================================================================================================================================#
# function adding_specificities_informations() : adding informations about the specificity of the triplets of primers in file primers_triplets_sorted.csv #
#=========================================================================================================================================================#

def adding_specificities_informations() :

  if debug :
    print_msg("adding_specificities_informations()")

  # called by testing_primers_triplets_specificities() when argument -species_db has been defined
  # executed in folder SNP_i/FORWARD|REVERSE
  # no triplets at all : primers_triplets.csv is empty -> empty primers_triplets_sorted.csv
  # triplets + no predicted amplification : triplet_i_specificity_ampli.csv and triplet_i_best_ampli.csv are empty
  # global unmodified variables
  # current_working_directory_tmp

  # local variables
  class_common                   = 0
  class_fam                      = 0
  class_hex                      = 0
  fl_primers_triplets_sorted     = 0
  fl_primers_triplets_sorted_tmp = 0
  fl_triplet_best_ampli          = 0
  fl_triplet_specif_primer       = 0
  flag_file                      = 0
  flag_found                     = 0
  flag_common                    = 0
  flag_fam                       = 0
  flag_hex                       = 0
  i                              = 0
  ident_common                   = ""
  ident_fam                      = ""
  ident_hex                      = ""
  line_1                         = ""
  line_2                         = ""
  line_no_1                      = 0
  line_no_2                      = 0
  lst_field_1                    = []
  lst_field_2                    = []
  scaffold                       = ""
  scaffold_common                = ""
  scaffold_fam                   = ""
  scaffold_hex                   = ""
 
  try :
    os.rename("primers_triplets_sorted.csv", "primers_triplets_sorted_tmp.csv")
  except :
    error(12, current_working_directory_tmp + "/primers_triplets_sorted.csv")
    return

  try :
    fl_primers_triplets_sorted_tmp = open("primers_triplets_sorted_tmp.csv", 'r')
  except :
    error(6, current_working_directory_tmp + "/primers_triplets_sorted_tmp.csv")

  try :
    fl_primers_triplets_sorted = open("primers_triplets_sorted.csv",'w')
  except :
    error(7, current_working_directory_tmp + "/primers_triplets_sorted.csv")

  line_no_1 = 0

  while 1 :
    flag_found = 0
    line_1     = fl_primers_triplets_sorted_tmp.readline()

    if line_1 == "" :
      # EOF ...
      break
    
    line_no_1 += 1
    line_1     = chop(line_1)
    if not len(line_1) : 
      continue

    if len(line_1) and line_no_1 == 1 :
      # skipping first line of fl_primers_triplets_sorted_tmp (headers)
      continue

    # data line
    # 0           ;1          ;2        ;3                        ;4    ;5  ;6    ;7   ;8       ;9       ;10                       ;11   ;12 ;13    ;14  ;15      ;16      ;17              ;18                          ;19   ;20 ;21    ;22    ;23      ;24      ;25              ;26             ;27
    #           id;orientation;triplet  ;common primer            ;start;len;tm   ;%gc ;self_any;self_end;FAM specific primer      ;start;len;tm    ;%gc ;self_any;self_end;FAM pair's score;HEX specific primer         ;start;len;tm    ;%gc   ;self_any;self_end;HEX pair's score;triplet's score;triplet's class
    # UN00132_1351;    REVERSE;triplet_1;AACCTTGGCAATCTGTTCGACGAAC;4    ; 25;64.18;48.0;29.27   ;29.27   ;AATTATCTCGTTGGCCGGGCATTTC;84   ;25 ;64.169;48.0;22.05   ;14.97   ;3.651           ;ACTAATTATCTCGTTGGCCGGGCATTTA;87   ;28 ;64.688;42.857;15.33   ;5.93    ;6.132           ;9.783          ;2

    lst_field_1 = line_1.split(';')

    try :
      fl_triplet_best_ampli = open(lst_field_1[2] + "_best_ampli.csv", 'r')
    except :
      error(6, current_working_directory_tmp + "/" + lst_field_1[2] + "_best_ampli.csv")

    line_no_2 = 0
    while 1 :
      line_2 = fl_triplet_best_ampli.readline()

      if line_2 == "" :
        # EOF ...
        break
    
      line_no_2 += 1
      line_2 = chop(line_2)
      if not len(line_2) : 
        continue

      if len(line_2) and line_no_2 == 1 :
        # skipping first line of fl_triplet_best_ampli (headers)
        continue
      
      # data line
      # 0           ;1          ;2        ;3       ;4                ;5           ;6              ;7              ;8               ;9                      ;10                            ;11                                         ;12                      ;13             ;14           ;15              ;16                   ;17                 ;18                  ;19                                      ;20                         ;21                       ;22          ;23        ;24           ;25                ;26              ;27               ;28                                      ;29                      ;30                       ;31          ;32        ;33           ;34                ;35              ;36              ;37                                       ;38  
      # id          ;orientation;triplet  ;scaffold;start on scaffold;SNP position;end on scaffold;scaffold length;size of amplicon;class for amplification;distribution for amplification;comments                                   ;sequence of common      ;start of common;end of common;length of common;orientation of common;identity for common;HSP class for common;distribution for common                 ;primer antiscore for common;sequence of FAM          ;start of FAM;end of FAM;length of FAM;orientation of FAM;identity for FAM;HSP class for FAM;distribution for FAM                    ;primer antiscore for FAM;sequence of HEX          ;start of HEX;end of HEX;length of HEX;orientation of HEX;identity for HEX;HSP class for HEX;distribution for HEX                    ;primer antiscore for HEX
      # UN00132_1351;REVERSE    ;triplet_4;Chr00   ;350471088        ;350471111   ;350471282      ;714758103      ;195             ;1-                     ;2 x 1-                        ;Several strong HSPs for common primer, ... ;ACCTTGGCAATCTGTTCGACGAAC;350471088      ;350471111    ;24              ;'+'                  ;I=24/24 (100%)     ;1                   ;4 x 1, 0 x 2, 0 x 3, 0 x 4, 0 x 5, 0 x 6;30                         ;AATTATCTCGTTGGCCGGGCATTTC;350471258   ;350471282 ;25           ;'-'               ;I=25/25 (100%)  ;1                ;2 x 1, 1 x 2, 0 x 3, 0 x 4, 0 x 5, 0 x 6;20                      ;AATTATCTCGTTGGCCGGGCATTTA;350471258   ;350471282 ;25           ;'-'               ;I=25/25 (100%)  ;1                ;1 x 1, 2 x 2, 0 x 3, 0 x 4, 0 x 5, 0 x 6;20

      lst_field_2 = line_2.split(';')

      if not flag_file :
        fl_primers_triplets_sorted.write("id;orientation;triplet;" + \
                                           "common primer;start;len;tm;%gc;self_any;self_end;" + \
                                           "FAM specific primer;start;len;tm;%gc;self_any;self_end;FAM pair's score;" + \
                                           "HEX specific primer;start;len;tm;%gc;self_any;self_end;HEX pair's score;" + \
                                           "triplet's score;triplet's class;" + \
                                           "identity for common;HSP class for common;identity for FAM;HSP class for FAM;identity for HEX;HSP class for HEX;" + \
                                           "scaffold;SNP position;size of amplicon;orientation of FAM/HEX;class for amplification;distribution for amplification;comments\n")
        flag_file = 1
        
      fl_primers_triplets_sorted.write(line_1 + ";" + \
                                         lst_field_2[17] + ";" + lst_field_2[18] + ";" + lst_field_2[26] + ";" + lst_field_2[27] + ";" + lst_field_2[35] + ";" + lst_field_2[36] + ";" + \
                                         lst_field_2[3]  + ";" + lst_field_2[5]  + ";" + lst_field_2[8]  + ";" + lst_field_2[25] + ";" + lst_field_2[9]  + ";" + lst_field_2[10] + ";" + lst_field_2[11] + "\n")
      flag_found = 1

    # only one line is expected for each triplet ...
    fl_triplet_best_ampli.close()

    if not flag_found :
      # case where no amplification has been predicted
      # looking for lacking data in triplet_i_specificity_primer.csv

      try :
        fl_triplet_specif_primer = open(lst_field_1[2] + "_specificity_primer.csv", 'r')
      except :
        error(6, current_working_directory_tmp + "/" + lst_field_1[2] + "_specificity_primer.csv")

      line_no_2       = 0
      class_common    = 0
      class_fam       = 0
      class_hex       = 0
      flag_common     = 0
      flag_fam        = 0
      flag_hex        = 0
      ident_common    = ""
      ident_fam       = ""
      ident_hex       = ""
      scaffold_common = ""
      scaffold_fam    = ""
      scaffold_hex    = ""

      while 1 :
        line_2 = fl_triplet_specif_primer.readline()

        if line_2 == "" :
          # EOF ...
          break
    
        line_no_2 += 1
        line_2 = chop(line_2)
        if not len(line_2) : 
          continue

        if len(line_2) and line_no_2 == 1 :
          # skipping first line of fl_triplet_specif_primer (headers)
          continue

        lst_field_2 = line_2.split(';')

        # the first one is the best
        if lst_field_2[3] == "common" :
          if not flag_common :
            scaffold_common = lst_field_2[5]
            ident_common    = lst_field_2[14]
            class_common    = lst_field_2[15]
            flag_common     = 1
        elif lst_field_2[3] == "fam" :
          if not flag_fam :
            scaffold_fam = lst_field_2[5]
            ident_fam    = lst_field_2[14]
            class_fam    = lst_field_2[15]
            flag_fam     = 1
        elif lst_field_2[3] == "hex" :
          if not flag_hex :
            scaffold_hex = lst_field_2[5]
            ident_hex    = lst_field_2[14]
            class_hex    = lst_field_2[15]
            flag_hex     = 1

      # all lines of triplet_i_specificity_primer.csv have been read
      fl_triplet_specif_primer.close()
      if not len(ident_common) :
        ident_common = "no HSP"
      if not len(ident_fam) :
        ident_fam = "no HSP"
      if not len(ident_hex) :
        ident_hex = "no HSP"
      scaffold = scaffold_common
      if scaffold.find(scaffold_fam) == -1 :
        scaffold += ", " + scaffold_fam
      if scaffold.find(scaffold_hex) == -1 :
        scaffold += ", " + scaffold_hex
      #Info : se base sur sa pour constuire le fichier all_primers...
      if not flag_file :
        fl_primers_triplets_sorted.write("id;orientation;triplet;" + \
                                           "common primer;start;len;tm;%gc;self_any;self_end;" + \
                                           "FAM specific primer;start;len;tm;%gc;self_any;self_end;FAM pair's score;" + \
                                           "HEX specific primer;start;len;tm;%gc;self_any;self_end;HEX pair's score;" + \
                                           "triplet's score;triplet's class;" + \
                                           "identity for common;HSP class for common;identity for FAM;HSP class for FAM;identity for HEX;HSP class for HEX;" + \
                                           "scaffold;SNP position;size of amplicon;orientation of FAM/HEX;class for amplification;distribution for amplification;comments\n")
        flag_file = 1

      fl_primers_triplets_sorted.write(line_1 + ";" + \
                                         ident_common + ";" + str(class_common) + ";" + ident_fam + ";" + str(class_fam) + ";" + ident_hex + ";" + str(class_hex) + ";" + \
                                         scaffold + ";0;0;?;0;;no predicted amplification\n")

  fl_primers_triplets_sorted_tmp.close()
  os.remove("primers_triplets_sorted_tmp.csv")

  fl_primers_triplets_sorted.close()

  # These informations are consequently added in file SNP/all_primers_triplets.csv
  # (see writing_all_primers_triplets_files())



#======================================================#
# function chop(s) : trimming \n at the end of a string #
#======================================================#

def chop(s) :

  if type(s) == type('a') and len(s) > 0 :
    if s[-1] == '\n' :
      if len(s) > 1 and s[-2] == '\r' :
        return s[:-2]
      else :
        return s[:-1]

  return s



#======================================#
# function error() : to process errors #
#======================================#

def error(no, s = "") :

  lst_errors = [ \
    (1, "Non integer argument used in error call (", ").", 1),
    (2, "Unknown error number (", ").", 1),
    (3, "Wrong list of arguments ", "(use -h or -H argument to get information).", 1),
    (4, "Mandatory argument ", " is missing (use -h or -H argument to get information).", 1),
    (5, "Impossible to open file '", "' with mode 'a (append)'.", 1),
    (6, "Impossible to open file '", "' with mode 'r (read)'.", 1),
    (7, "Impossible to open file '", "' with mode 'w (write)'.", 1),
    (8, "The directory '", "' doesn't exist.", 1),
    (9, "A directory ", " exists already in the current working directory. Please delete or rename it before relaunching this program", 1),
    (10, "Wrong value in PCR parameters ", ".", 1),
    (11, "Wrong flanking sequence for SNP ", ".", 0),
    (12, "File ", " is empty, probably because primer3 fails.", 0),
    (13, "Wrong value for argument ", ".", 1),
    (14, "File ", " doesn't exist, probably because primer3 fails.", 0)]

  no_error = 0
  label   = ""

  if type(no) == type(1) :

    if no < 1 or no > len(lst_errors) :
      no_error = 2
      label   = str(no)

    else :
      no_error = no
      label   = s

  else :
    no_error = 1
    label   = no

  t_error = lst_errors[no_error - 1]

  if fl_err :
    if t_error[3] :
      fl_err.write("\nError %d :\n" % (no_error))
    else :
      fl_err.write("\nWarning %d :\n" % (no_error))
    fl_err.write("%s\n" % (t_error[1] + label + t_error[2]))
    fl_err.flush()
  else :
    if t_error[3] :
      sys.stderr.write("\nError %d :\n" % (no_error))
    else :
      sys.stderr.write("\nWarning %d :\n" % (no_error))
    sys.stderr.write("%s\n" % (t_error[1] + label + t_error[2]))
    sys.stderr.flush()

  if t_error[3] == 0 :
    return
  
  sys.exit(1)



#====================================================#
# function files_close() : to close all opened files #
#====================================================#

def files_close() :

  if fl_input :
    fl_input.close()

  if fl_pcr_parameters :
    fl_pcr_parameters.close()

  if fl_err :
    fl_err.close ()

  return



#======================================================================================#
# function getting_default_pcr_parameters() : to display the PCR parameters by default #
#======================================================================================#

def getting_default_pcr_parameters() :

  line = ""
  print("opane parameter file : ",fl_pcr_parameters_default_name)
  # fl_pcr_parameters_default_name : file containing the default PCR parameters
  try :
    fl_pcr_parameters_default = open(fl_pcr_parameters_default_name, 'r')
  except :
    error(6, fl_pcr_parameters_default_name)

  while 1 :
    line = fl_pcr_parameters_default.readline()

    if line == "" :
      # EOF ...
      break
    
    print line[:-1]

  if fl_pcr_parameters_default :
    fl_pcr_parameters_default.close()

  sys.exit(0)



#============================================================#
# function help() : to display information about the program #
#============================================================#

def help() :

  print """kaspar_primers_triplets_design_v_0_2.py [-h] [-H] [-pcr_parameters_default] -in f1 [-folder d1] [-flanking_sequence_min_size n1] [-checked_zone_length n2] [-pcr_parameters_file f2] [-pcr_parameters p1 p2 ...] [-species_db db1] [-max_primer n3] [-max_triplet n4] [-max_N_global n5] [-max_N_local n6] [-max_Ambi_global n7] [-max_Ambi_local n8] [-err f3] [-debug]
  This program designs several triplets of primers.
  Arguments:
  -h                             : to get some information about this program (what you get now)
  -H                             : to get detailed information about this program
  -pcr_parameters_default        : to get the PCR parameters by default
  -in f1                         : file containing a list of SNP with their backward and forward sequences
                                   mandatory
  -folder d1                     : path of the directory that will be created in the current directory and
                                   where results will be written
                                   optional - default value: "SNP"
  -flanking_sequence_min_size l1 : minimum length of the flanking sequences
                                   optional - default value: 50
  -checked_zone_length l2        : neither N, nor ambiguous IUPAC code among the l2 nucleotides before and after the SNP in a sequence
                                   optional - default value: 10 - give 0 to avoid this control
  -pcr_parameters_file f2        : file containing the PCR parameters used instead of the default PCR parameters
                                   optional - default value: none
  -pcr_parameters p1 p2 ...      : PCR parameters used instead of the default PCR parameters
                                   optional - default value: none
  -species_db db1                : path to the data base of the species used to check the specificities of the triplets of primers
                                   optional - default value: none
  -max_primer n3                 : maximal count of pairs of primers selected from Primer3 output files
                                   optional - default value: value of PRIMER_NUM_RETURN in PCR parameters (default for Primer3: 10)
  -max_triplet n4                : maximal count of triplets per direction, FORWARD or REVERSE
                                   optional - default value: 5
  -max_N_global n5               : maximum count of N for the whole flanking sequence
                                   optional - default value: 0
  -max_N_local n6                : maximum count of N for the part of the flanking sequence where primers can be located
                                   optional - default value: 0
  -max_Ambi_global n7            : maximum count of ambiguous IUPAC codes for the whole flanking sequence
                                   optional - default value: 0
  -max_Ambi_local n8             : maximum count of ambiguous IUPAC codes for the part of the flanking sequence where primers can be located
                                   optional - default value: 0
  -err f3                        : errors file
                                   optional - if not defined standard error is used

  -debug                         : debug mode
 """
  sys.exit(0)



#=====================================================================#
# function help_detailed() : to display information about the program #
#=====================================================================#

def help_detailed() :

  print """kaspar_primers_triplets_design_v_0_2.py [-h] [-H] [-pcr_parameters_default] -in f1 [-folder d1] [-flanking_sequence_min_size n1] [-checked_zone_length n2] [-pcr_parameters_file f2] [-pcr_parameters p1 p2 ...] [-species_db db1] [-max_primer n3] [-max_triplet n4] [-max_N_global n5] [-max_N_local n6] [-max_Ambi_global n7] [-max_Ambi_local n8] [-err f3] [-debug]

  This program designs several triplets of primers.

  Arguments:
  -h                             : to get some information about this program (what you get now)
  -H                             : to get detailed information about this program
  -pcr_parameters_default        : to get the PCR parameters by default
  -in f1                         : file containing a list of SNP with their backward and forward sequences
                                   mandatory
  -folder d1                     : path of the directory that will be created in the current directory and
                                   where results will be written
                                   optional - default value: "SNP"
  -flanking_sequence_min_size l1 : minimum length of the flanking sequences
                                   optional - default value: 50
  -checked_zone_length l2        : neither N, nor ambiguous IUPAC code among the l2 nucleotides before and after the SNP in a sequence
                                   optional - default value: 10 - give 0 to avoid this control
  -pcr_parameters_file f2        : file containing the PCR parameters used instead of the default PCR parameters
                                   optional - default value: none
  -pcr_parameters p1 p2 ...      : PCR parameters used instead of the default PCR parameters
                                   optional - default value: none
  -species_db db1                : path to the data base of the species used to check the specificities of the triplets of primers
                                   optional - default value: none
  -max_primer n3                 : maximal count of pairs of primers selected from Primer3 output files
                                   optional - default value: value of PRIMER_NUM_RETURN in PCR parameters (default for Primer3: 10)
  -max_triplet n4                : maximal count of triplets per direction, FORWARD or REVERSE
                                   optional - default value: 5
  -max_N_global n5               : maximum count of N for the whole flanking sequence
                                   optional - default value: 0
  -max_N_local n6                : maximum count of N for the part of the flanking sequence where primers can be located
                                   optional - default value: 0
  -max_Ambi_global n7            : maximum count of ambiguous IUPAC codes for the whole flanking sequence
                                   optional - default value: 0
  -max_Ambi_local n8             : maximum count of ambiguous IUPAC codes for the part of the flanking sequence where primers can be located
                                   optional - default value: 0
  -err f3                        : errors file
                                   optional - if not defined standard error is used

  -debug                         : debug mode

This program executes a set of tasks that aim to define triplets of primers adapted to KASPar genotyping :
1. It reads an input file f1 ('-in f1') that contains flanking sequences of SNP, such as :
   >header_sequence_1
   GTGGCTGTTCTACCCATTTCCAGAGGTGACAGTTGATTTGGGGAAAATGAGTGTGTTAGG[A/G]AAGAAGATTAAGTTAATTAAGTTCATTGTAACAATTTTATTTAGTAAATTTAACATCATA
   >header_sequence_2
   NNNTAGCAGATATGCTGCAGAAAAGTTTAAAGAGAAAGAGGTACTTAATT[T/G]TCTTGGATGATATCTGGAGTTGTGMARTGTGGGATGGCGTGAGACGATGC

2. It checks if each input sequence respects the rules defined by default or by the user through a set of parameters
   [-flanking_sequence_min_size n1]
   [-checked_zone_length n2]
   [-max_N_global n5]
   [-max_N_local n6]
   [-max_Ambi_global n7]
   [-max_Ambi_local n8]

3. It creates for each sequence a folder (SNP_1, SNP_2, ...) where all results will be kept.
   Each folder contains 2 sub-folders, FORWARD and REVERSE :
   - FORWARD folder contains results obtained with the parameter of Primer 3 'SEQUENCE_FORCE_LEFT_END'
     primers FAM and HEX are on the strand of the flanking sequence and end at the SNP position
     the common primer is on the reverse strand somewhere on the right flanking sequence
   - REVERSE folder contains results obtained with the parameter of Primer 3 'SEQUENCE_FORCE_RIGHT_END'
     primers FAM and HEX are on the reverse strand of the flanking sequence and begin at the SNP position
     the common primer is on the strand of the flanking sequence, somewhere on the left flanking sequence

4. It launches Primer3 with a set of parameters that the user may easily adapt to his own needs with a few arguments:
   -pcr_parameters_file f2
   -pcr_parameters p1 p2 ...
   To know the values of the default parameters used by Primer3, please execute: 
      kaspar_primers_triplets_design.py -pcr_parameters_default
   Files produced:
   - fam_primer3_input.txt
   - fam_primer3_output.txt
   - hex_primer3_input.txt
   - hex_primer3_output.txt

5. It processes Primer3 output files to search best triplets (Common, FAM, HEX).
   Results of this step depend on:
   - the maximal count of pairs of primers retained be Primer3, that can be personalized with:
     - the parameter 'PRIMER_NUM_RETURN' of Primer3
     - the parameter [-max_primer n3] of kaspar_primers_triplets_design.py
   - the maximal count of triplets, that can be personalized with:
     - the parameter [-max_triplet n4] of kaspar_primers_triplets_design.py
   Files produced:
   - fam_primer3_output_parsed.csv
   - hex_primer3_output_parsed.csv
   - primers_triplets.csv        : one line per pair of primers
   - primers_triplets_sorted.csv : one line per triplet, the first triplet should be the best. Fields:
     + identification : id; orientation; triplet
     + common primer  : sequence; start; len; tm; %gc; self_any; self_end
     + FAM primer     : sequence; start; len; tm; %gc; self_any; self_end; FAM pair's score
     + HEX primer     : sequence; start; len; tm; %gc; self_any; self_end; HEX pair's score
     + triplet's score
     + triplet's class

   Original information:
   - "triplet's score" is just the sum of the PRIMER_PAIR_n_PENALTY calculated by Primer3 for the 2 pairs (FAM, Common) and (HEX, Common)
     higher values correspond with bigger differences from optimal parameters of the PCR given to Primer3
   - "triplet's class" depends on triplet's score :
       score    class
       <  5     1  the best one
       < 10     2
       < 15     3
       < 20     4
       >= 20    5

6. If [-species_db db1] is defined, the program tries to assess the specificity of the triplets and the quality of the predicted amplification.
   It performs a blastn of the sequences of the 3 primers against the db1 database.
   Then it tries to analyze the set of HSP identified for each primer to assess how it can link itself to the genome.
   At last, it tries to find which couples of HSP could lead to a PCR product. Common anf FAM or HEX must be an the same scaffold, with different strands and with a distance compatible with an amplification.
 
   Results of this step depend on the PCR parameters given to Primer3, PRIMER_PRODUCT_SIZE_RANGE and PRIMER_PRODUCT_OPT_SIZE.

   Files produced:
   - triplet_1.fasta                      : sequences of the 3 primers 
   - triplet_1_vs_species_ref.bltn        : output of blastn
   - triplet_1_vs_species_ref.bltn.filtre : main data for each HSP (several lines per HSP)
   - triplet_1_specificity_primer.csv     : description of HSP (one line per HSP)
   - triplet_1_specificity_ampli.csv      : description of potential predicted amplifications (one line per predicted amplification)
   - triplet_1_best_ampli.csv             : description of the best potential predicted amplification

   File updated:
   - primers_triplets_sorted.csv with new fields :
     + identity for common
     + HSP class for common
     + identity for FAM
     + HSP class for FAM
     + identity for HEX
     + HSP class for HEX
     + scaffold
     + SNP position
     + size of amplicon
     + orientation of FAM/HEX
     + class for amplification
     + distribution for amplification
     + comments

   Original information:
   - "HSP class"
         1     perfect alignment        : start 1,   end  = length of primer    , identity = 100%
         2     nearly perfect alignment : start 1,   end  = length of primer - 1, identity = 100%
         3     good alignment           : start < 5, end  = length of primer    , identity inside HSP >= 95%, identity for whole primer >= 90%
         4     nearly good alignment    : start < 5, end  = length of primer - 1, identity inside HSP >= 95%, identity for whole primer >= 90%
         5     medium alignment         : start < 9, end >= length of primer - 1, identity inside HSP >= 90%, identity for whole primer >= 85%
         6     bad alignment
     A perfect alignment of a triplet corresponds with class 1 for the common and the FAM or HEX primer, and class 2 for the third primer (HEX or FAM).
     
   - "distribution for Common, FAM or HEX" : count of HSP of class 1, of HSP of class 2, ...
     0 x 1, 18 x 2, 0 x 3, 11 x 4, 0 x 5, 57 x 6      (a desesperate case ...)
   - primer antiscore for Common, FAM or HEX : calculated from the distribution of HSP classes for a primer.
     The stronger is the value of this primer antiscore, the worse it is.
   - "class for amplification" : an integer from 1 to 6 that may be followed by a '+' or a '-'
     The integer depends on the class of the Common primer and of the best class for the FAM and HEX primer
           fh\c  1 2 3 4 5
              1  1 2 3 4 4 
              2  2 3 4 4 4
              3  3 4 4 5 5
              4  4 4 5 5 6
              5  4 4 5 6 6
     This integer is followed by a "+' when the predicted size of the amplicon is between the minimal and the maximal sizes defined for Primer3.
     This integer is followed by a "-' when the predicted size of the amplicon is greater than a value calculated from optimal and maximal sizes that cannot be greater than 500.
   - "distribution for amplification" : count of "class for amplification" for all predicted amplifications with the triplet
   - "Comments" :
     Only one strong HSP for common primer, Several strong HSPs for FAM primer, Several strong HSPs for HEX primer

   The quality of a triplet may be assessed by the combination of both values, "class for amplification" and "distribution for amplification". A case where a triplet has a class of "1+" but a distribution of "2 x 1+, 3 x 2, 2 x 5+" should be avoided !

7. At last the program gathers data obtained for each SNP in 2 or 3 files :
   -  nb_triplets_by_snp.csv counts how many triplets have been identified
      If [-species_db db1] is defined, it counts also how many triplets correspond with at least one predicted amplification
      id;folder;total of triplets;forward;reverse;total with amplification;forward with amplification;reverse with amplification 
   
   - all_primers_triplets.csv gives data for each identified triplets of primers
     id;orientation;triplet;
     common primer;start;len;tm;%gc;self_any;self_end;
     FAM specific primer;start;len;tm;%gc;self_any;self_end;FAM pair's score;
     HEX specific primer;start;len;tm;%gc;self_any;self_end;HEX pair's score;
     triplet's score;triplet's class;
     identity for common;HSP class for common;
     identity for FAM;HSP class for FAM;
     identity for HEX;HSP class for HEX;
     scaffold;SNP position;size of amplicon;orientation of FAM/HEX;class for amplification;distribution for amplification;comments

   - primers_triplets_specificities.csv describe each predicted best amplification
     id;orientation;triplet;
     scaffold;start on scaffold;SNP position;end on scaffold;scaffold length;size of amplicon;class for amplification;distribution for amplification;comments;
     sequence of common;start of common;end of common;length of common;orientation of common;identity for common;HSP class for common;distribution for common;primer antiscore for common;
     sequence of FAM;start of FAM;end of FAM;length of FAM;orientation of FAM;identity for FAM;HSP class for FAM;distribution for FAM;primer antiscore for FAM;
     sequence of HEX;start of HEX;end of HEX;length of HEX;orientation of HEX;identity for HEX;HSP class for HEX;distribution for HEX;primer antiscore for HEX

 """
  sys.exit(0)



#==========================================================================================================================#
# function primer3_parser(fl_primer3_output_name,fl_primer3_output_parser_name,direction) : to parse a Primer3 output file #
#==========================================================================================================================#

def primer3_parser(fl_primer3_output_name,fl_primer3_output_parser_name,direction) :

  if debug :
    print_msg("primer3_parser(" + fl_primer3_output_name + ", " + fl_primer3_output_parser_name + ", " + direction + ")")
    os.system("pwd")

  try :
    fl_input = open(fl_primer3_output_name, 'r')
  except :
    error(6, current_working_directory_tmp + "/" + fl_primer3_output_name)

  try :
    fl_output = open(fl_primer3_output_parser_name, 'w')
  except :
    error(7, current_working_directory_tmp + "/" + fl_primer3_output_parser_name)

  line      = ""
  lst_field = []

  fl_output.write("id;oligo;common primer;start;len;tm;%gc;self_any;self_end;specific primer;start;len;tm;%gc;self_any;self_end;pair's score \n")

  oligo_number = 0

  while 1 :
    line = fl_input.readline()

    if line == "" :
      # EOF ...
      break

    if line.find("SEQUENCE_ID") == 0 :
      sequence_id = chop(line.split("=")[1])

    if line.find("PRIMER_PAIR_" + str(oligo_number) + "_PENALTY") == 0 :
      pair_penalty = round(float(chop(line.split("=")[1])),3)

    if line.find("PRIMER_LEFT_" + str(oligo_number) + "_SEQUENCE") == 0 :
      if direction == "FORWARD" :
        specific_sequence = chop(line.split("=")[1])
      else :
        common_sequence = chop(line.split("=")[1])

    if line.find("PRIMER_RIGHT_" + str(oligo_number) + "_SEQUENCE") == 0 :
      if direction == "FORWARD" :
        common_sequence = chop(line.split("=")[1])
      else :
        specific_sequence = chop(line.split("=")[1])

    if line.find("PRIMER_LEFT_" + str(oligo_number) + "=") == 0 :
      lst_field_01 = line.split("=")
      lst_field_02 = lst_field_01[1].split(",")
      if direction == "FORWARD" :
        specific_start = chop(lst_field_02[0])
        specific_length = chop(lst_field_02[1])
      else :
        common_start = chop(lst_field_02[0])
        common_length = chop(lst_field_02[1])

    if line.find("PRIMER_RIGHT_" + str(oligo_number) + "=") == 0 :
      lst_field_01 = line.split("=")
      lst_field_02 = lst_field_01[1].split(",")
      if direction == "FORWARD" :
        common_start = chop(lst_field_02[0])
        common_length = chop(lst_field_02[1])
      else :
        specific_start = chop(lst_field_02[0])
        specific_length = chop(lst_field_02[1])

    if line.find("PRIMER_LEFT_" + str(oligo_number) + "_TM") == 0 :
      if direction == "FORWARD" :
        specific_tm = round(float(chop(line.split("=")[1])),3)
      else :
        common_tm = round(float(chop(line.split("=")[1])),3)

    if line.find("PRIMER_RIGHT_" + str(oligo_number) + "_TM") == 0 :
      if direction == "FORWARD" :
        common_tm = round(float(chop(line.split("=")[1])),3)
      else :
        specific_tm = round(float(chop(line.split("=")[1])),3)

    if line.find("PRIMER_LEFT_" + str(oligo_number) + "_GC_PERCENT") == 0 :
      if direction == "FORWARD" :
        specific_gc = round(float(chop(line.split("=")[1])),3)
      else :
        common_gc = round(float(chop(line.split("=")[1])),3)

    if line.find("PRIMER_RIGHT_" + str(oligo_number) + "_GC_PERCENT") == 0 :
      if direction == "FORWARD" :
        common_gc = round(float(chop(line.split("=")[1])),3)
      else :
        specific_gc = round(float(chop(line.split("=")[1])),3)

    if line.find("PRIMER_LEFT_" + str(oligo_number) + "_SELF_ANY") == 0 :
      if direction == "FORWARD" :
        specific_any = round(float(chop(line.split("=")[1])),3)
      else :
        common_any = round(float(chop(line.split("=")[1])),3)

    if line.find("PRIMER_RIGHT_" + str(oligo_number) + "_SELF_ANY") == 0 :
      if direction == "FORWARD" :
        common_any = round(float(chop(line.split("=")[1])),3)
      else :
        specific_any = round(float(chop(line.split("=")[1])),3)

    if line.find("PRIMER_LEFT_" + str(oligo_number) + "_SELF_END") == 0 :
      if direction == "FORWARD" :
        specific_end = round(float(chop(line.split("=")[1])),3)
      else :
        common_end = round(float(chop(line.split("=")[1])),3)

    if line.find("PRIMER_RIGHT_" + str(oligo_number) + "_SELF_END") == 0 :
      if direction == "FORWARD" :
        common_end = round(float(chop(line.split("=")[1])),3)
      else :
        specific_end = round(float(chop(line.split("=")[1])),3)

      fl_output.write(sequence_id + ";OLIGO_" + str(oligo_number) + ";" + \
                        common_sequence + ";" + common_start + ";" + common_length + ";" + str(common_tm) + ";" + str(common_gc) + ";" + str(common_any) + ";" + str(common_end) + ";" + \
                        specific_sequence + ";" + specific_start + ";" + specific_length + ";" + str(specific_tm) + ";" + str(specific_gc) + ";" + str(specific_any) + ";" + str(specific_end) + ";" + \
                        str(pair_penalty) + "\n")

      oligo_number += 1



#============================================================================================================#
# function primers_triplet_specificity(i ,j ,direction) : testing the specificity of the triplet j for SNP i #
#============================================================================================================#

def primers_triplet_specificity(i, j, direction) :
  # SNP i, triplet j, direction FORWARD|REVERSE

  if debug :
    print_msg("primers_triplet_specificity(" + str(i) + ", " + str(j) + ", " + direction + ")")

  global  amp_size_limit_2, current_working_directory_tmp, forward_nb_triplet_amp, reverse_nb_triplet_amp

  # local variables
  alert_msg                  = ""
  amp_class                  = ""
  amp_size                   = 0
  count_hsp                  = 0
  dct_distrib                = {}
  dct_seq_primer             = {}
  distrib_amp                = ""
  distrib_com                = ""
  distrib_fam                = ""
  distrib_hex                = ""
  fam_class                  = 0
  fam_dir                    = ""
  fam_end                    = 0
  fam_ident                  = ""
  fam_start                  = 0
  fl_triplet_best_ampli      = 0    # triplet_i_best_ampli.csv (with global assessment)
  fl_triplet_fasta           = 0    # triplet_i.fasta
  fl_triplet_filtre          = 0    # triplet_i_vs_species_ref.bltn.filtre
  fl_triplet_specif_primer   = 0    # triplet_i_specificity_primer.csv (assessing all HSP for all )
  fl_triplet_specif_ampli    = 0    # triplet_i_specificity_ampli.csv (assessing all possible amplifications)
  flag_ampli                 = 0
  flag_fam                   = 0
  flag_file_specif           = 0
  flag_found                 = 0
  flag_triplet               = 1
  hex_class                  = 0
  hex_dir                    = ""
  hex_end                    = 0
  hex_ident                  = ""
  hex_start                  = 0
  hsp_class                  = 0
  hsp_length                 = 0
  id_snp                     = ""
  ident_nb                   = 0
  ident_pct_1                = 0
  ident_pct_2                = 0
  identity                   = ""
  k                          = 0
  k_best                     = 0
  l                          = 0
  line                       = ""
  lst_amp_class              = []
  lst_ampli                  = []
  lst_common                 = []
  lst_distrib                = []
  lst_fam                    = []
  lst_fam_hex                = []
  lst_field                  = []
  lst_hex                    = []
  lst_triplet                = []
  primer_class               = 0     # integer between 1:6 + 0 when no primer was designed
  primer_score_com           = 0     # for common primer - the stronger is primer_score, the worse it is
  primer_score_fam           = 0     # for FAM primer
  primer_score_hex           = 0     # for HEX primer
  primer_type                = ""
  query_dir                  = ""
  query_end                  = 0
  query_length               = 0
  query_start                = 0
  scaffold                   = ""
  scaffold_end               = 0
  scaffold_length            = 0
  scaffold_start             = 0
  snp_path                   = ""
  state                      = 0
  subject_dir                = ""
  subject_end                = 0
  subject_length             = 0
  subject_start              = 0
  tpl                        = ()
  triplet                    = "triplet_" + str(j)

  # retrieving sequences of primers
  # -------------------------------
      
  snp_path = "SNP_" + str(i + 1)

  try : 
    fl_triplet_fasta = open(triplet + ".fasta", 'r')
  except :
    error(6, current_working_directory + dir_name + "/" + snp_path + "/" + direction + "/" + triplet + ".fasta")

  while 1 :
    line = fl_triplet_fasta.readline()

    if line == "" :
      # EOF ...
      break
    
    line = chop(line)
    line = line.strip()

    if not len(line) :
      continue
    if line[0] == ">" :
      if line.find("common") > 0 :
        primer_type = "common"
      elif line.find("FAM") > 0 :
        primer_type = "fam"
      elif line.find("HEX") > 0 :
        primer_type = "hex"
      else :
        primer_type = "?"
    else :
      # sequence
      dct_seq_primer[primer_type] = line

  fl_triplet_fasta.close()

  # launching blastn and filtre_blastall
  # ------------------------------------

  if debug :
    print_msg(path_blastall + " -p blastn -d " + species_db_name + " -i " + triplet + ".fasta -F F -e 1.0 -b 10 -v 10 -o " + triplet + "_vs_species_ref.bltn")
    #print_msg("blastn -d " + species_db_name + " -i " + triplet + ".fasta -F F -e 1.0 -b 10 -v 10 -o " + triplet + "_vs_species_ref.bltn")
  os.system(path_blastall + " -p blastn -d " + species_db_name + " -i " + triplet + ".fasta -F F -e 1.0 -b 10 -v 10 -o " + triplet + "_vs_species_ref.bltn")
  #modif
  #os.system("blastn -d " + species_db_name + " -i " + triplet + ".fasta -F F -e 1.0 -b 10 -v 10 -o " + triplet + "_vs_species_ref.bltn")
  
  if debug :
    print_msg(path_filtre_blastall + " -nb 5 -all < " + triplet + "_vs_species_ref.bltn > triplet_" + str(j) + "_vs_species_ref.bltn.filtre")
  os.system(path_filtre_blastall + " -nb 2 -all < " + triplet + "_vs_species_ref.bltn > triplet_" + str(j) + "_vs_species_ref.bltn.filtre")

  # processing triplet_j_vs_species_ref.bltn.filtre file
  # ----------------------------------------------------

  # all HSP are retrieved
  try :
    fl_triplet_filtre = open(triplet + "_vs_species_ref.bltn.filtre", 'r')
  except :
    error(6, current_working_directory + dir_name + "/SNP_" + str(i) + "/" + direction + "/" + triplet + "_vs_species_ref.bltn.filtre")

  # writing in file triplet_j_specificity_primer.csv
  # ------------------------------------------------
  # in dir_name/SNP_i/FORWARD and dir_name/SNP_i/REVERSE (i = SNP's index, j = triplet's index)
  try :
    fl_triplet_specif_primer = open(triplet + "_specificity_primer.csv", 'w')
  except :
    error(7, current_working_directory_tmp + "/" + triplet + "_specificity_primer.csv")

  while 1 :
    line = fl_triplet_filtre.readline()
    #print("line --- : ",line)
    if line == "" :
      # EOF ...
      break
    
    line = chop(line)
    line = line.strip()

    if not len(line) :
      state = 0
      continue
    #print("state",state)
    if state == 0 :
      # Query: UN00020_639 common primer

      if line.find("common") > 0 :
        primer_type  = "common"
      elif line.find("FAM") > 0 :
        primer_type  = "fam"
      elif line.find("HEX") > 0 :
        primer_type  = "hex"

      id_snp = line.split(' ')[1]
      state   = 1

    elif state == 1 :
      # Sbjct: Chr00 2012-12-18#BGI
      #print("line",line)
      if len(line.split(' ')) > 1:
        scaffold = line.split(' ')[1]
      state = 2

    elif state == 2 :
      # blastn // Sbjct : 82313774 82313800 714758103 + // Query : 1 27 27 + // S=54.0 E=2e-06 I=27/27 (100%) Pos=27/27 (100%)

      lst_field = line.split(' ')
      #print("lst_field",lst_field)
      subject_start  = int(lst_field[4])
      subject_end    = int(lst_field[5])
      subject_length = int(lst_field[6])
      subject_dir    = lst_field[7]
      query_start    = int(lst_field[11])
      query_end      = int(lst_field[12])
      query_length   = int(lst_field[13])
      query_dir      = lst_field[14]
      identity       = lst_field[18] + " " + lst_field[19]
      ident_nb       = int(lst_field[18][2:lst_field[18].find('/')])
      hsp_length     = int(lst_field[18][lst_field[18].find('/') + 1:])
      ident_pct_1    = int(lst_field[19][1:-2])
      ident_pct_2    = ident_nb * 100 / query_length

      # Assessing hsp_class
      if query_start == 1 and query_end == query_length and ident_pct_1 == 100 :
        hsp_class = 1
      elif query_start == 1 and query_end == query_length - 1 and ident_pct_1 == 100 :
        hsp_class = 2
      elif query_start < 5 and query_end == query_length and ident_pct_1 >= 95 and ident_pct_2 >= 90 :
        hsp_class = 3
      elif query_start < 5 and query_end == query_length - 1 and ident_pct_1 >= 95 and ident_pct_2 >= 90 :
        hsp_class = 4
      elif query_start < 9 and (query_length - query_end) < 2 and ident_pct_1 >= 90 and ident_pct_2 >= 85 :
        hsp_class = 5
      else :
        hsp_class = 6
      
      if not flag_file_specif :
        fl_triplet_specif_primer.write("id;orientation;triplet;primer;sequence;" + \
                                         "scaffold;start on scaffold;end on scaffold;scaffold length;orientation on scaffold;" + \
                                         "start on primer;end on primer;primer length;orientation on primer;identity;HSP class\n")

        flag_file_specif = 1

      #TODO à modifier "direction"
      sens = ""
      if direction == "FORWARD" :
        sens = "SENS"
      else:
        sens = "ANTI_SENS"
      fl_triplet_specif_primer.write(id_snp + ";" + sens + ";" + triplet + ";" + primer_type + ";" + dct_seq_primer[primer_type] + ";" + \
                                     scaffold + ";" + str(subject_start) + ";" + str(subject_end) + ";" + str(subject_length) + ";'" + subject_dir + "';" + \
                                     str(query_start) + ";" + str(query_end) + ";" + str(query_length) + ";'" + query_dir + "';" + identity + ";" + str(hsp_class) + "\n") 

      #      0       1        2            3          4                            5          6             7            8               9            10           11         12            13         14        15
      tpl = (id_snp, direction, triplet, primer_type, dct_seq_primer[primer_type], scaffold, subject_start, subject_end, subject_length, subject_dir, query_start, query_end, query_length, query_dir, identity, hsp_class)
      if primer_type == "common" :
        lst_common.append(tpl)
      elif primer_type == "fam" :
        lst_fam.append(tpl)
      elif primer_type == "hex" :
        lst_hex.append(tpl)

  fl_triplet_filtre.close()
  fl_triplet_specif_primer.close()
 
  # Looking at HSP distribution of hsp_class ... and trying to conclude
  # -------------------------------------------------------------------

  if not len(lst_common) :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "No HSP for common primer"
    flag_triplet = 0

  if not len(lst_fam) :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "No HSP for FAM primer"

  if not len(lst_hex) :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "No HSP for HEX primer"

  if not len(lst_fam) and not len(lst_hex) :
    flag_triplet = 0
    
  if not flag_triplet :
    # Creating an empty file triplet_i_specificity_ampli.csv
    try :
      fl_triplet_specif_ampli = open(triplet + "_specificity_ampli.csv", 'w')
    except :
      error(7, triplet + "_specificity_ampli.csv")
    fl_triplet_specif_ampli.close()

    try :
      fl_triplet_best_ampli = open(triplet + "_best_ampli.csv", 'w')
    except :
      error(7, triplet + "_best_ampli.csv")
    fl_triplet_best_ampli.close()

    return

  # Common
  lst_distrib = [0, 0, 0, 0, 0, 0]
  for k in range(len(lst_common)) :
    # count of hsp_class 1, count of hsp_class 2, ... count of hsp_class 6
    # 1-> [0], 2 -> [1] ...
    lst_distrib[lst_common[k][15] - 1] += 1

  distrib_com = ""
  for k in range(len(lst_distrib)) :
    if k > 0 :
      distrib_com += ", "
    distrib_com += str(lst_distrib[k]) + " x " + str(k + 1)

  # primer_score : the stronger is primer_score, the worse it is
  count_hsp        = 0
  primer_score_com = 0
  if lst_distrib[0] + lst_distrib[1] > 0 :
    # HSP class 1 and 2
    count_hsp = 1
    primer_score_com += 10 * (lst_distrib[0] + lst_distrib[1] - count_hsp)
  if lst_distrib[2] + lst_distrib[3] > 0 :
    # HSP class 2 and 3
    if count_hsp :
      primer_score_com += 5 * (lst_distrib[2] + lst_distrib[3])
    else :
      count_hsp = 1
      primer_score_com += 5 * (lst_distrib[2] + lst_distrib[3] - count_hsp)
  if lst_distrib[4] > 0 :
    # HSP class 4
    if count_hsp :
       primer_score_com += 2 * (lst_distrib[4])
    else :
      count_hsp = 1
      primer_score_com += 2 * (lst_distrib[4] - count_hsp)
  if lst_distrib[5] > 0 :
    # HSP class 5
    if count_hsp :
       primer_score_com += 1 * (lst_distrib[5])
    else :
      count_hsp = 1
      primer_score_com += 1 * (lst_distrib[5] - count_hsp)
  # HSP class 6 : not taken in account

  # lst_distrib[0] : count of 1, lst_distrib[1] : count of 2, ...
  if (lst_distrib[0] + lst_distrib[1] + lst_distrib[2] + lst_distrib[3]) < 1 :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "No strong HSP for common primer"
  elif (lst_distrib[0] + lst_distrib[1] + lst_distrib[2] + lst_distrib[3]) == 1 :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "Only one strong HSP for common primer"
  else :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "Several strong HSPs for common primer"

  # FAM
  lst_distrib = [0, 0, 0, 0, 0, 0]
  for k in range(len(lst_fam)) :
    # count of hsp_class 1, count of hsp_class 2, ... count of hsp_class 6
    lst_distrib[lst_fam[k][15] - 1] += 1

  distrib_fam = ""
  for k in range(len(lst_distrib)) :
    if k > 0 :
      distrib_fam += ", "
    distrib_fam += str(lst_distrib[k]) + " x " + str(k + 1)

  # primer_score : the stronger is primer_score, the worse it is
  count_hsp        = 0
  primer_score_fam = 0
  if lst_distrib[0] + lst_distrib[1] > 0 :
    # HSP class 1 and 2
    count_hsp = 1
    primer_score_fam += 10 * (lst_distrib[0] + lst_distrib[1] - count_hsp)
  if lst_distrib[2] + lst_distrib[3] > 0 :
    # HSP class 2 and 3
    if count_hsp :
      primer_score_fam += 5 * (lst_distrib[2] + lst_distrib[3])
    else :
      count_hsp = 1
      primer_score_fam += 5 * (lst_distrib[2] + lst_distrib[3] - count_hsp)
  if lst_distrib[4] > 0 :
    # HSP class 4
    if count_hsp :
      primer_score_fam += 2 * (lst_distrib[4])
    else :
      count_hsp = 1
      primer_score_fam += 2 * (lst_distrib[4] - count_hsp)
  if lst_distrib[5] > 0 :
    # HSP class 5
    if count_hsp :
      primer_score_fam += 1 * (lst_distrib[5])
    else :
      count_hsp = 1
      primer_score_fam += 1 * (lst_distrib[5] - count_hsp)
  # HSP class 6 : not taken in account

  # lst_distrib[0] : count of 1, lst_distrib[1] : count of 2, ...
  if (lst_distrib[0] + lst_distrib[1] + lst_distrib[2] + lst_distrib[3]) < 1 :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "No strong HSP for FAM primer"
  elif (lst_distrib[0] + lst_distrib[1] + lst_distrib[2] + lst_distrib[3]) == 1 :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "Only one strong HSP for FAM primer"
  else :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "Several strong HSPs for FAM primer"

  # HEX
  lst_distrib = [0, 0, 0, 0, 0, 0]
  for k in range(len(lst_hex)) :
    # count of hsp_class 1, count of hsp_class 2, ... count of hsp_class 6
    lst_distrib[lst_hex[k][15] - 1] += 1

  distrib_hex = ""
  for k in range(len(lst_distrib)) :
    if k > 0 :
      distrib_hex += ", "
    distrib_hex += str(lst_distrib[k]) + " x " + str(k + 1)

  # primer_score : the stronger is primer_score, the worse it is
  count_hsp        = 0
  primer_score_hex = 0
  if lst_distrib[0] + lst_distrib[1] > 0 :
    # HSP class 1 and 2
    count_hsp = 1
    primer_score_hex += 10 * (lst_distrib[0] + lst_distrib[1] - count_hsp)
  if lst_distrib[2] + lst_distrib[3] > 0 :
    # HSP class 2 and 3
    if count_hsp :
      primer_score_hex += 5 * (lst_distrib[2] + lst_distrib[3])
    else :
      count_hsp = 1
      primer_score_hex += 5 * (lst_distrib[2] + lst_distrib[3] - count_hsp)
  if lst_distrib[4] > 0 :
    # HSP class 4
    if count_hsp :
      primer_score_hex += 2 * (lst_distrib[4])
    else :
      count_hsp = 1
      primer_score_hex += 2 * (lst_distrib[4] - count_hsp)
  if lst_distrib[5] > 0 :
    # HSP class 5
    if count_hsp :
      primer_score_hex += 1 * (lst_distrib[5])
    else :
      count_hsp = 1
      primer_score_hex += 1 * (lst_distrib[5] - count_hsp)
  # HSP class 6 : not taken in account

  # lst_distrib[0] : count of 1, lst_distrib[1] : count of 2, ...
  if (lst_distrib[0] + lst_distrib[1] + lst_distrib[2] + lst_distrib[3]) < 1 :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "No strong HSP for HEX primer"
  elif (lst_distrib[0] + lst_distrib[1] + lst_distrib[2] + lst_distrib[3]) == 1 :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "Only one strong HSP for HEX primer"
  else :
    if len(alert_msg) :
      alert_msg += ", "
    alert_msg += "Several strong HSPs for HEX primer"

  # if debug and i == 1 and j == 1 :
  #   print_msg(alert_msg)

  # Looking for correspondances between lst_fam and lst_hex
  # -------------------------------------------------------

  if not len(lst_fam) :
    for k in range(len(lst_hex)) :
      lst_fam_hex.append((-1, k))
  
  if not len(lst_hex) :
    for k in range(len(lst_fam)) :
      lst_fam_hex.append((k, -1))

  if not len(lst_fam_hex) :
    for k in range(len(lst_fam)) :
      flag_found = 0
      for l in range(len(lst_hex)) :
        # same scaffold    : lst_fam[k][5] lst_hex[l][5]
        # same orientation : lst_fam[k][9] lst_hex[l][9]
        # same SNP position : depends on orientation
        # + : subject_end + query_length - query_end
        # - : subject_end - query_length + query_end
        if lst_fam[k][5] == lst_hex[l][5] and lst_fam[k][9] == lst_hex[l][9] :
          if lst_fam[k][9] == '+' :
            if lst_fam[k][7] + lst_fam[k][12] - lst_fam[k][11] == lst_hex[l][7] + lst_hex[l][12] - lst_hex[l][11] :
              # Yes !
              lst_fam_hex.append((k, l))
              flag_found = 1
              break
          else :
            # lst_fam[k][9] == '-'
            if lst_fam[k][7] - lst_fam[k][12] + lst_fam[k][11] == lst_hex[l][7] - lst_hex[l][12] + lst_hex[l][11] :
              # Yes !
              lst_fam_hex.append((k, l))
              flag_found = 1
              break
      # may be not found
      if not flag_found :
        lst_fam_hex.append((k, -1))

    # may be original ones from lst_hex
    for k in range(len(lst_hex)) :
      flag_found = 0
      for l in range(len(lst_fam)) :
        # same scaffold    : lst_fam[k][5] lst_hex[l][5]
        # same orientation : lst_fam[k][9] lst_hex[l][9]
        # same SNP position : depends on orientation
        # + : subject_end + query_length - query_end
        # - : subject_end - query_length + query_end
        if lst_hex[k][5] == lst_fam[l][5] and lst_hex[k][9] == lst_fam[l][9] :
          if lst_hex[k][9] == '+' :
            if lst_hex[k][7] + lst_hex[k][12] - lst_hex[k][11] == lst_fam[l][7] + lst_fam[l][12] - lst_fam[l][11] :
              # Yes !
              if (l, k) not in lst_fam_hex :
                lst_fam_hex.append((l, k))
              flag_found = 1
              break
          else :
            # lst_hex[k][9] == '-'
            if lst_hex[k][7] - lst_hex[k][12] + lst_hex[k][11] == lst_fam[l][7] - lst_fam[l][12] + lst_fam[l][11] :
              # Yes !
              if (l, k) not in lst_fam_hex :
                lst_fam_hex.append((l, k))
              flag_found = 1
              break
      # may be not found
      if not flag_found :
        lst_fam_hex.append((-1, k))
  
  # if debug and i == 1 and j == 1 :
  #   print_msg("lst_common " + str(lst_common))
  #   print_msg("lst_fam " + str(lst_fam))
  #   print_msg("lst_fam_hex " + str(lst_fam_hex))

  # Assessing each couple of primers : common and best of (fam, hex)
  # writing in file triplet_j_specificity_primer.csv
  # ----------------------------------------------------------------
  # in dir_name/SNP_i/FORWARD and dir_name/SNP_i/REVERSE (i = SNP's index, j = triplet's index)

  try :
    fl_triplet_specif_ampli = open(triplet + "_specificity_ampli.csv", 'w')
  except :
    error(7, triplet + "_specificity_ampli.csv")
  
  flag_file_specif = 0

  for k in range(len(lst_common)) :
    # HSP for common primer
    for l in range(len(lst_fam_hex)) :
      flag_ampli = 0
      # which is best : fam or hex ?
      flag_fam = 0

      if lst_fam_hex[l][0] == -1 :
        flag_fam = 0
      elif lst_fam_hex[l][1] == -1 :
        flag_fam = 1
      else :
        # 10 : query_start , 11 : query_end (could be more tricky with identity ...)
        if lst_fam[lst_fam_hex[l][0]][11] - lst_fam[lst_fam_hex[l][0]][10] >= lst_hex[lst_fam_hex[l][1]][11] - lst_hex[lst_fam_hex[l][1]][10] :
          flag_fam = 1

      # criteria to assess amplification : same scaffold, consistent positions and directions, class of common and fam or hex primers
      # scaffold = field 5, orientation = field 9
      # tuple in lst_ampli : k (common), ...
      # if debug and i == 1 and j == 1 :
      #   print_msg("flag_fam : " + str(flag_fam))

      if flag_fam :
        # FAM should be the best aligned
        # scaffold : field 5 , subject_dir : field 9
        if lst_common[k][5] == lst_fam[lst_fam_hex[l][0]][5]  and lst_common[k][9] != lst_fam[lst_fam_hex[l][0]][9] :
          flag_ampli      = 1
          scaffold        = lst_common[k][5]
          scaffold_length = lst_common[k][8]
          # could be good : assessment of the amplicon
          # subject_start : field 6 , subject_end : field 7 , hsp_class : field 15
          # default : PRIMER_PRODUCT_SIZE_RANGE=62-85, PRIMER_PRODUCT_OPT_SIZE=75
          scaffold_end   = max(lst_common[k][6], lst_common[k][7], lst_fam[lst_fam_hex[l][0]][6], lst_fam[lst_fam_hex[l][0]][7])
          scaffold_start = min(lst_common[k][6], lst_common[k][7], lst_fam[lst_fam_hex[l][0]][6], lst_fam[lst_fam_hex[l][0]][7])
          amp_size       = scaffold_end - scaffold_start + 1
          if lst_common[k][9] == "+" :
            # lst_fam[lst_fam_hex[l][0]][9] == "-"
            # subject_end (7) - query_length (12) + query_end (11)
            snp_position = lst_fam[lst_fam_hex[l][0]][7] - lst_fam[lst_fam_hex[l][0]][12] + lst_fam[lst_fam_hex[l][0]][11]
          else :
            # lst_fam[lst_fam_hex[l][0]][9] == "+"
            # subject_end (7) + query_length (12) - query_end (11)
            snp_position = lst_fam[lst_fam_hex[l][0]][7] + lst_fam[lst_fam_hex[l][0]][12] - lst_fam[lst_fam_hex[l][0]][11]
          hsp_class_c  = lst_common[k][15]
          hsp_class_fh = lst_fam[lst_fam_hex[l][0]][15]

      else :
        # HEX should be the best aligned
        if lst_common[k][5] == lst_hex[lst_fam_hex[l][1]][5]  and lst_common[k][9] != lst_hex[lst_fam_hex[l][1]][9] :
          flag_ampli      = 1
          scaffold        = lst_common[k][5]
          scaffold_length = lst_common[k][8]
          # could be good : assessment of the amplicon
          # subject_start : field 6 , subject_end : field 7 , hsp_class : field 15
          # default : PRIMER_PRODUCT_SIZE_RANGE=62-85, PRIMER_PRODUCT_OPT_SIZE=75
          scaffold_end   = max(lst_common[k][6], lst_common[k][7], lst_hex[lst_fam_hex[l][1]][6], lst_hex[lst_fam_hex[l][1]][7])
          scaffold_start = min(lst_common[k][6], lst_common[k][7], lst_hex[lst_fam_hex[l][1]][6], lst_hex[lst_fam_hex[l][1]][7])
          amp_size       = scaffold_end - scaffold_start + 1
          if lst_common[k][9] == "+" :
            # lst_hex[lst_fam_hex[l][1]][9] == "-"
            # subject_end (7) - query_length (12) + query_end (11)
            snp_position = lst_hex[lst_fam_hex[l][1]][7] - lst_hex[lst_fam_hex[l][1]][12] + lst_hex[lst_fam_hex[l][1]][11]
          else :
            # lst_hex[lst_fam_hex[l][1]][9] == "+"
            # subject_end (7) + query_length (12) - query_end (11)
            snp_position = lst_hex[lst_fam_hex[l][1]][7] + lst_hex[lst_fam_hex[l][1]][12] - lst_hex[lst_fam_hex[l][1]][11]
          hsp_class_c  = lst_common[k][15]
          hsp_class_fh = lst_hex[lst_fam_hex[l][1]][15]
       
      amp_class = "0"
      if flag_ampli :
        if amp_size <= amp_size_limit_1 and hsp_class_c != 6 and hsp_class_fh != 6 :
          # amplification should be possible
          # assessing amplification quality ...
          # fh\c  1 2 3 4 5
          # 
          #    1  1 2 3 4 4 
          #    2  2 3 4 4 4
          #    3  3 4 4 5 5
          #    4  4 4 5 5 6
          #    5  4 4 5 6 6
          if hsp_class_c == 1 and hsp_class_fh == 1 :
            amp_class = "1"
          elif (hsp_class_c == 1 and hsp_class_fh == 2) or (hsp_class_c == 2 and hsp_class_fh == 1) :
            amp_class = "2"
          elif (hsp_class_c == 1 and hsp_class_fh == 3) or (hsp_class_c == 1 and hsp_class_fh == 3) or (hsp_class_c == 2 and hsp_class_fh == 2) :
            amp_class = "3"
          elif (hsp_class_c == 5 and hsp_class_fh in (4, 5)) or (hsp_class_c in (4, 5) and hsp_class_fh == 5) :
            amp_class = "6"
          elif (hsp_class_c == 3 and hsp_class_fh in (4, 5)) or (hsp_class_c == 4 and hsp_class_fh in (3, 4)) or (hsp_class_c == 5 and hsp_class_fh == 3) :
            amp_class = "5"
          else :
            amp_class = "4"

          if amp_size >= amp_size_min and amp_size <= amp_size_max :
            amp_class += "+"
          elif amp_size > amp_size_limit_2 :
            amp_class += "-"

        else :
          # not (amp_size <= amp_size_limit_1 and hsp_class_c != 6 and hsp_class_fh != 6)
          flag_ampli = 0
      # if debug and i == 1 and j == 1 :
      #   print_msg("flag_ampli : " + str(flag_ampli) + " - amp_class : " + amp_class)

      if flag_ampli :
        if not flag_file_specif :
          fl_triplet_specif_ampli.write("id;orientation;triplet;" + \
                                          "scaffold;start on scaffold;SNP position;end on scaffold;scaffold length;size of amplicon;class for amplification;" + \
                                          "sequence of common;start of common;end of common;length of common;orientation of common;identity for common;HSP class for common;distribution for common;primer antiscore for common;" + \
                                          "sequence of FAM;start of FAM;end of FAM;length of FAM;orientation of FAM;identity for FAM;HSP class for FAM;distribution for FAM;primer antiscore for FAM;" + \
                                          "sequence of HEX;start of HEX;end of HEX;length of HEX;orientation of HEX;identity for HEX;HSP class for HEX;distribution for HEX;primer antiscore for HEX\n")
          flag_file_specif = 1

        if lst_fam_hex[l][0] == -1 :
          fam_start        = -1
          fam_end          = -1
          fam_dir          = "no HSP"
          fam_ident        = ""
          fam_class        = -1
          distrib_fam      = ""
          primer_score_fam = -1
        else :
          fam_start = min(lst_fam[lst_fam_hex[l][0]][6], lst_fam[lst_fam_hex[l][0]][7])
          fam_end   = max(lst_fam[lst_fam_hex[l][0]][6], lst_fam[lst_fam_hex[l][0]][7])
          fam_dir   = lst_fam[lst_fam_hex[l][0]][9]
          fam_ident = lst_fam[lst_fam_hex[l][0]][14]
          fam_class = lst_fam[lst_fam_hex[l][0]][15]

        if lst_fam_hex[l][1] == -1 :
          hex_start        = -1
          hex_end          = -1
          hex_dir          = "no HSP"
          hex_ident        = ""
          hex_class        = -1
          distrib_hex      = ""
          primer_score_hex = -1
        else :
          hex_start = min(lst_hex[lst_fam_hex[l][1]][6], lst_hex[lst_fam_hex[l][1]][7])
          hex_end   = max(lst_hex[lst_fam_hex[l][1]][6], lst_hex[lst_fam_hex[l][1]][7])
          hex_dir   = lst_hex[lst_fam_hex[l][1]][9]
          hex_ident = lst_hex[lst_fam_hex[l][1]][14]
          hex_class = lst_hex[lst_fam_hex[l][1]][15]

        fl_triplet_specif_ampli.write(id_snp + ";" + direction + ";" + triplet + ";" + \
                                        scaffold + ";" + str(scaffold_start) + ";" + str(snp_position) + ";" + str(scaffold_end) + ";" + str(scaffold_length) + ";" + str(amp_size) + ";" + amp_class + ";" + \
                                        dct_seq_primer["common"] + ";" + str(min(lst_common[k][6], lst_common[k][7])) + ";" + str(max(lst_common[k][6], lst_common[k][7])) + ";" + str(len(dct_seq_primer["common"])) + ";'" + lst_common[k][9] + "';" + lst_common[k][14] + ";" + str(lst_common[k][15]) + ";" + distrib_com + ";" + str(primer_score_com) + ";" + \
                                        dct_seq_primer["fam"] + ";" + str(fam_start) + ";" + str(fam_end) + ";" + str(len(dct_seq_primer["fam"])) + ";'" + fam_dir +  "';" + fam_ident + ";" + str(fam_class)  +  ";" + distrib_fam + ";" + str(primer_score_fam) + ";" + \
                                        dct_seq_primer["hex"] + ";" + str(hex_start) + ";" + str(hex_end) + ";" + str(len(dct_seq_primer["hex"])) + ";'" + hex_dir +  "';" + hex_ident + ";" + str(hex_class)  +  ";" + distrib_hex + ";" + str(primer_score_hex) + "\n")

        #        0       1          2 
        # tpl = (id_snp, direction, triplet, \
        #          3         4               5             6             7                8         9
        #          scaffold, scaffold_start, snp_position, scaffold_end, scaffold_length, amp_size, amp_class, \
        #          10                        11 start of common                       12 end of common                         13 length of common            14 orientation    15 identity        16 HSP class       17           18
        #          dct_seq_primer["common"], min(lst_common[k][6], lst_common[k][7]), max(lst_common[k][6], lst_common[k][7]), len(dct_seq_primer["common"]), lst_common[k][9], lst_common[k][14], lst_common[k][15], distrib_com, primer_score_com, \
        #          19                     20         21       22                          23       24         25         26           27
        #          dct_seq_primer["fam"], fam_start, fam_end, len(dct_seq_primer["fam"]), fam_dir, fam_ident, fam_class, distrib_fam, primer_score_fam, \
        #          28                     29         30       31                          32       33         34         35           36
        #          dct_seq_primer["hex"], hex_start, hex_end, len(dct_seq_primer["hex"]), hex_dir, hex_ident, hex_class, distrib_hex, primer_score_hex) 

        tpl = (id_snp, direction, triplet, \
                 scaffold, scaffold_start, snp_position, scaffold_end, scaffold_length, amp_size, amp_class, \
                 dct_seq_primer["common"], min(lst_common[k][6], lst_common[k][7]), max(lst_common[k][6], lst_common[k][7]), len(dct_seq_primer["common"]), lst_common[k][9], lst_common[k][14], lst_common[k][15], distrib_com, primer_score_com, \
                 dct_seq_primer["fam"], fam_start, fam_end, len(dct_seq_primer["fam"]), fam_dir, fam_ident, fam_class, distrib_fam, primer_score_fam, \
                 dct_seq_primer["hex"], hex_start, hex_end, len(dct_seq_primer["hex"]), hex_dir, hex_ident, hex_class, distrib_hex, primer_score_hex) 

        lst_ampli.append(tpl)

  fl_triplet_specif_ampli.close()

  # global assessment of triplet from lst_ampli
  # -------------------------------------------

  lst_amp_class = ["1+","1","1-","2+","2","2-","3+","3","3-","4+","4","4-","5+","5","5-","6+","6","6-"]
  # looking for best ampli
  k_best = 0

  for k in range(1, len(lst_ampli)) :
    # 1. amp_class
    # 2. if same amp_class, lower (2 x primer_score_com + primer_score_fam + primer_score_hex)
    if lst_amp_class.index(lst_ampli[k][9]) < lst_amp_class.index(lst_ampli[k_best][9]) :
      k_best = k
    elif lst_amp_class.index(lst_ampli[k][9]) == lst_amp_class.index(lst_ampli[k_best][9]) :
      if ((2 * lst_ampli[k][18]) + lst_ampli[k][27] + lst_ampli[k][36]) <  ((2 * lst_ampli[k_best][18]) + lst_ampli[k_best][27] + lst_ampli[k_best][36]) :
        k_best = k

  # lst_ampli[9] amp_class : 1+, 1, 1-, 2+, 2, 2-, ... 6-
  # distrib_amp 
  dct_distrib = {}
  for k in range(len(lst_ampli)) :
    if lst_ampli[k][9] not in dct_distrib :
      dct_distrib[lst_ampli[k][9]] = 1
    else :
      dct_distrib[lst_ampli[k][9]] += 1
  
  distrib_amp = ""
  for amp_class in lst_amp_class :
    if amp_class in dct_distrib :
      if len(distrib_amp) :
        distrib_amp += ", "
      distrib_amp += str(dct_distrib[amp_class]) + " x " + amp_class


  try :
    fl_triplet_best_ampli = open(triplet + "_best_ampli.csv", 'w')
  except :
    error(7, triplet + "_best_ampli.csv")

  flag_file_specif = 0

  if len(lst_ampli) :
    
    if not flag_file_specif :
      fl_triplet_best_ampli.write("id;orientation;triplet;" + \
                                    "scaffold;start on scaffold;SNP position;end on scaffold;scaffold length;size of amplicon;class for amplification;distribution for amplification;comments;" + \
                                    "sequence of common;start of common;end of common;length of common;orientation of common;identity for common;HSP class for common;distribution for common;primer antiscore for common;" + \
                                    "sequence of FAM;start of FAM;end of FAM;length of FAM;orientation of FAM;identity for FAM;HSP class for FAM;distribution for FAM;primer antiscore for FAM;" + \
                                    "sequence of HEX;start of HEX;end of HEX;length of HEX;orientation of HEX;identity for HEX;HSP class for HEX;distribution for HEX;primer antiscore for HEX\n")
      flag_file_specif = 1
    #TODO orientation
    sens = ""
    if direction == "FORWARD" :
      sens = "SENS"
    else:
      sens = "ANTI_SENS"
    fl_triplet_best_ampli.write(id_snp + ";" + sens + ";" + triplet + ";" + \
                                  lst_ampli[k_best][3] + ";" + str(lst_ampli[k_best][4]) + ";" + str(lst_ampli[k_best][5]) + ";" + str(lst_ampli[k_best][6]) + ";" + str(lst_ampli[k_best][7]) + ";" + str(lst_ampli[k_best][8]) + ";" + lst_ampli[k_best][9] + ";" + distrib_amp + ";" + alert_msg + ";" + \
                                  lst_ampli[k_best][10] + ";" + str(lst_ampli[k_best][11]) + ";" + str(lst_ampli[k_best][12]) + ";" + str(lst_ampli[k_best][13]) + ";'" + lst_ampli[k_best][14] + "';" + lst_ampli[k_best][15] + ";" + str(lst_ampli[k_best][16]) + ";" + lst_ampli[k_best][17] + ";" + str(lst_ampli[k_best][18]) + ";" + \
                                  lst_ampli[k_best][19] + ";" + str(lst_ampli[k_best][20]) + ";" + str(lst_ampli[k_best][21]) + ";" + str(lst_ampli[k_best][22]) + ";'" + lst_ampli[k_best][23] + "';" + lst_ampli[k_best][24] + ";" + str(lst_ampli[k_best][25]) + ";" + lst_ampli[k_best][26] + ";" + str(lst_ampli[k_best][27]) + ";" + \
                                  lst_ampli[k_best][28] + ";" + str(lst_ampli[k_best][29]) + ";" + str(lst_ampli[k_best][30]) + ";" + str(lst_ampli[k_best][31]) + ";'" + lst_ampli[k_best][32] + "';" + lst_ampli[k_best][33] + ";" + str(lst_ampli[k_best][34]) + ";" + lst_ampli[k_best][35] + ";" + str(lst_ampli[k_best][36]) + "\n")

    # updating forward_nb_triplet_amp and reverse_nb_triplet_amp
    if direction == "FORWARD" :
      forward_nb_triplet_amp[i] += 1
    else :
      reverse_nb_triplet_amp[i] += 1

  fl_triplet_best_ampli.close()

  # end of function primers_triplet_specificity(i ,j ,direction)



#=================================================================================================#
# function primers_triplets_design() : to design one or several triplets of primers for each SNP  #
#=================================================================================================#

def primers_triplets_design() :

  if debug :
    print_msg("primers_triplets_design()")

  print_msg("\nVerification of the flanking sequences.")

  # global variables
  global current_working_directory_tmp, nb_snp, orientation

  # global unmodified variables

  # local variables
  fl_nb_triplets_by_snp = 0          # file nb_triplets_by_snp.csv
  line                  = ""
  lst_field             = []
  snp_path              = ""

  # creation of the directory SNP in the current working directory
  if os.path.exists(current_working_directory + dir_name) :
    # shutil.rmtree(current_working_directory + dir_name)
    error(9, dir_name)
  os.mkdir(current_working_directory + dir_name)

  # writing the Primer3 input files
  writing_primer3_input_files()
  nb_snp = no_snp

  os.chdir(current_working_directory + dir_name)
  current_working_directory_tmp = current_working_directory + dir_name

  # file including the number of triplets by SNP
  try :
    fl_nb_triplets_by_snp = open("nb_triplets_by_snp.csv",'w')
  except :
    error(7, current_working_directory_tmp + "/nb_triplets_by_snp.csv")

  fl_nb_triplets_by_snp.write("id;folder;total;forward;reverse\n")

  if debug :
    print_msg("nb_snp : " + str(nb_snp))
    print_msg("snp_processing : " + str(snp_processing))

  for i in range(nb_snp) :

    if snp_processing[i] in (1, 3) :
                
      orientation = "FORWARD"
      # snp_path = "SNP_" + str(i)
      snp_path = "SNP_" + str(i + 1)
      current_working_directory_tmp = current_working_directory + dir_name + "/" + snp_path + "/" + orientation
      os.chdir(current_working_directory_tmp)

      # executing Primer3 for the FAM and HEX alleles of the SNP
      os.system(path_primer3 + " -output=fam_primer3_output.txt < fam_primer3_input.txt")
      os.system(path_primer3 + " -output=hex_primer3_output.txt < hex_primer3_input.txt")

      # parsing the Primer3 output files
      primer3_parser("fam_primer3_output.txt", "fam_primer3_output_parsed.csv", orientation)
      primer3_parser("hex_primer3_output.txt", "hex_primer3_output_parsed.csv", orientation)

      # writing the file containing the triplets of primers from the 2 previous files
      primers_triplets_file_writing("fam_primer3_output_parsed.csv", "hex_primer3_output_parsed.csv")

      forward_nb_triplet.append(nb_triplet)  # nb_triplet is calculated in function primers_triplets_file_writing()

    else :
      forward_nb_triplet.append(0)

    # if snp_processing[i - 1] in (2, 3) :
    if snp_processing[i] in (2, 3) :

      orientation = "REVERSE"
      snp_path = "SNP_" + str(i + 1)
      current_working_directory_tmp = current_working_directory + dir_name + "/" + snp_path + "/" + orientation
      os.chdir(current_working_directory_tmp)

      # executing Primer3 for the FAM and HEX alleles of the SNP
      os.system(path_primer3 + " -output=fam_primer3_output.txt < fam_primer3_input.txt")
      os.system(path_primer3 + " -output=hex_primer3_output.txt < hex_primer3_input.txt")

      # parsing the Primer3 output files
      primer3_parser("fam_primer3_output.txt", "fam_primer3_output_parsed.csv", orientation)
      primer3_parser("hex_primer3_output.txt", "hex_primer3_output_parsed.csv", orientation)
      # writing the file containing the triplets of primers from the 2 previous files
      primers_triplets_file_writing("fam_primer3_output_parsed.csv", "hex_primer3_output_parsed.csv")

      reverse_nb_triplet.append(nb_triplet)  # nb_triplet is calculated in function primers_triplets_file_writing()

    else :
      reverse_nb_triplet.append(0)

    # if snp_processing[i - 1] > 0 :
    if snp_processing[i] > 0 :
      # both FORWARD and REVERSE
      os.chdir(current_working_directory + dir_name)
      current_working_directory_tmp = current_working_directory + dir_name
      # total_nb_triplet = forward_nb_triplet[i - 1] + reverse_nb_triplet[i - 1]
      total_nb_triplet = forward_nb_triplet[i] + reverse_nb_triplet[i]
      # fl_nb_triplets_by_snp.write(lst_snp_id[i - 1]  + ";" + snp_path + ";" + str(total_nb_triplet) + ";" + str(forward_nb_triplet[i - 1]) + ";" + str(reverse_nb_triplet[i - 1]) + "\n")
      fl_nb_triplets_by_snp.write(lst_snp_id[i]  + ";" + snp_path + ";" + str(total_nb_triplet) + ";" + str(forward_nb_triplet[i]) + ";" + str(reverse_nb_triplet[i]) + "\n")

    else :

      # forward_nb_triplet.append(0)
      # reverse_nb_triplet.append(0)
      # fl_nb_triplets_by_snp.write(lst_snp_id[i - 1] + ";eliminated;0;0;0\n")
      fl_nb_triplets_by_snp.write(lst_snp_id[i] + ";eliminated;0;0;0\n")

    # i += 1

  fl_nb_triplets_by_snp.close()



#============================================================================================================#
# function primers_triplets_file_writing() : creating a file with all selected triplets of primers for a SNP #
#============================================================================================================#

def primers_triplets_file_writing(fl_fam_primer3_output_parser_name, fl_hex_primer3_output_parser_name) :

  if debug :
    print_msg("primers_triplets_file_writing(" + fl_fam_primer3_output_parser_name + ", " + fl_hex_primer3_output_parser_name + ")")

  # fl_fam_primer3_output_parser_name : fam_primer3_output_parsed.csv
  # fl_hex_primer3_output_parser_name : hex_primer3_output_parsed.csv
  # called by primers_triplets_design() in folder SNP_i/FORWARD|REVERSE

  # global variables
  global nb_triplet

  # global unmodified variables
  # max_triplet
  # orientation

  # local variables
  category                  = 0
  fam_lst_field             = []
  fam_lst_field_02          = []
  fl_fam                    = 0
  flag_file                 = 0
  fl_hex                    = 0
  fl_triplet_fasta          = 0   # triplet_i.fasta
  fl_triplets_output        = 0
  fl_triplets_output_sorted = 0
  hex_lst_field             = []
  hex_lst_field_02          = []
  i                         = 0
  j                         = 0
  line                      = ""
  line_fam                  = ""
  line_hex                  = ""
  lst_reader                = []
  no_line                   = 0
  no_line_fam               = 0
  no_line_hex               = 0
  reader                    = 0
  row                       = ""
  score                     = 0.0

  # file containing the primers doublets for the FAM allele
  try :
    fl_fam = open(fl_fam_primer3_output_parser_name, 'r')
  except :
    error(6, fl_fam_primer3_output_parser_name)

  # file containing the triplets of primers
  try :
    fl_triplets_output = open("primers_triplets.csv", 'w')
  except :
    error(7, fl_triplets_output)

  nb_triplet  = 0
  no_line_fam = 0

  while 1 :
    line_fam = fl_fam.readline()
    #print(line_fam)
    if line_fam == "" :
      # EOF ...
      break

    no_line_fam += 1
    line_fam = chop(line_fam)

    if len(line_fam) and no_line_fam == 1 :
      continue

    if not len(line_fam) :
      continue

    fam_lst_field = line_fam.split(";OLIGO_")

    fam_lst_field_02 = fam_lst_field[1].split(";")

    # file containing the primers doublets for the HEX allele
    try :
      fl_hex = open(fl_hex_primer3_output_parser_name, 'r')
    except :
      error(6, fl_hex_primer3_output_parser_name)

    no_line_hex = 0

    while 1 :
      line_hex = fl_hex.readline()

      if line_hex == "" :
        # EOF ...

        fl_hex.close()
        break

      no_line_hex += 1
      line_hex = chop(line_hex)

      if len(line_hex) and no_line_hex == 1 :
        continue

      if not len(line_hex) :
        continue

      hex_lst_field = line_hex.split(";OLIGO_")

      hex_lst_field_02 = hex_lst_field[1].split(";")

      if fam_lst_field_02[1] == hex_lst_field_02[1] :  

        score = float(chop(fam_lst_field_02[15])) + float(chop(hex_lst_field_02[15]))

        if score >= 0. and score < 5. :
          category = 1
        elif score >= 5. and score < 10. :
          category = 2
        elif score >= 10. and score < 15. :
          category = 3
        elif score >= 15. and score < 20. :
          category = 4
        elif score >= 20. :
          category = 5

        if not flag_file :
          fl_triplets_output.write("id;orientation;" + \
                             "common primer;start;len;tm;%gc;self_any;self_end;" + \
                             "FAM specific primer;start;len;tm;%gc;self_any;self_end;FAM pair's score;" + \
                             "HEX specific primer;start;len;tm;%gc;self_any;self_end;HEX pair's score;" + \
                             "triplet's score;triplet's class\n")
          flag_file = 1
        #TODO modife apporter
        sens = ""
        if orientation == "FORWARD" :
          sens = "SENS"
        else:
          sens = "ANTI_SENS"
        fl_triplets_output.write(fam_lst_field[0] + ";" + sens + ";" + \
                                   fam_lst_field_02[1]  + ";" + fam_lst_field_02[2]  + ";" + fam_lst_field_02[3]  + ";" + fam_lst_field_02[4] + ";" + fam_lst_field_02[5]  + ";" + fam_lst_field_02[6]  + ";" + fam_lst_field_02[7]  + ";" + \
                                   fam_lst_field_02[8]  + ";" + fam_lst_field_02[9]  + ";" + fam_lst_field_02[10] + ";" + fam_lst_field_02[11] + ";" + fam_lst_field_02[12] + ";" + fam_lst_field_02[13] + ";" + fam_lst_field_02[14] + ";" + chop(fam_lst_field_02[15]) + ";" + \
                                   hex_lst_field_02[8]  + ";" + hex_lst_field_02[9]  + ";" + hex_lst_field_02[10] + ";" + hex_lst_field_02[11] + ";" + hex_lst_field_02[12] + ";" + hex_lst_field_02[13] + ";" + hex_lst_field_02[14] + ";" + chop(hex_lst_field_02[15]) + ";" + \
                                   str(score) + ";" + str(category) + "\n")

    fl_hex.close()

  fl_fam.close()
  fl_triplets_output.close()

  # Now : 1. sorting triplets of primers by increasing score (the best triplets having the lower score)
  #       2. adding column triplet
  try :
    reader = csv.reader(open('primers_triplets.csv'), delimiter=';')
  except :
    error(6, current_working_directory_tmp + "/primers_triplets.csv")

  # skipping line of headers
  try :
    row = reader.next() # first line of the csv file
  except :
    error(12, current_working_directory_tmp + "/primers_triplets.csv")
    return

  for row in reader : # from the second line to the end of the csv file
    lst_reader.append(row)

  lst_reader.sort(key=lambda v: float(v[25])) # sort by increasing score

  try :
    fl_triplets_output_sorted = open("primers_triplets_sorted.csv",'w')
  except :
    error(7, current_working_directory_tmp + "/primers_triplets_sorted.csv")

  flag_file = 0
  i         = 0
  while i < min(len(lst_reader), max_triplet) :  # the number of triplets in primers_triplets_sorted.csv is limited to the top 5 for each SNP
    # fl_triplets_output_sorted.write(';'.join(lst_reader[i]) + "\n")

    if len(species_db_name) :
      # editing fasta file for blast against genome reference
      fl_triplet_fasta = open("triplet_" + str(i + 1) + ".fasta",'w')
      fl_triplet_fasta.write(">" + lst_reader[i][0] + " common primer\n")
      fl_triplet_fasta.write(lst_reader[i][2] + "\n")
      fl_triplet_fasta.write(">" + lst_reader[i][0] + " FAM specific primer\n")
      fl_triplet_fasta.write(lst_reader[i][9] + "\n")
      fl_triplet_fasta.write(">" + lst_reader[i][0] + " HEX specific primer\n")
      fl_triplet_fasta.write(lst_reader[i][17] + "\n")
      fl_triplet_fasta.close()

    # preparing the line with new field triplet
    line = lst_reader[i][0] + ";" + lst_reader[i][1] + ";triplet_" + str(i + 1)
    for j in range(2, len(lst_reader[i])) :
      line += ";" + lst_reader[i][j]
    line += "\n"
    if not flag_file :
      fl_triplets_output_sorted.write("id;orientation;triplet;" + \
                                    "common primer;start;len;tm;%gc;self_any;self_end;" + \
                                    "FAM specific primer;start;len;tm;%gc;self_any;self_end;FAM pair's score;" + \
                                    "HEX specific primer;start;len;tm;%gc;self_any;self_end;HEX pair's score;" + \
                                    "triplet's score;triplet's class\n")
      flag_file = 1

    fl_triplets_output_sorted.write(line)

    i += 1

  # updating global nb_triplet
  nb_triplet = i

  fl_triplets_output_sorted.close()



#=======================================================================#
# function print_msg(msg) : to display information during the execution #
#=======================================================================#

def print_msg(msg = "\n") :
  if type(msg) == type('a') :
    if msg[len(msg) - 1] != '\n' :
      msg += '\n'
    if fl_err :
      fl_err.write(msg)
      fl_err.flush()
    else :
      sys.stderr.write(msg)
      sys.stderr.flush()

  return



#====================================================================================================================#
# function testing_primers_triplets_specificities() : to test the specificities of the triplets of primers for a SNP #
#====================================================================================================================#

def testing_primers_triplets_specificities() :

  if debug :
    print_msg("testing_primers_triplets_specificities()")

  # global lst_triplets_specificities
  global current_working_directory_tmp, orientation

  # global unmodified variables
  # forward_nb_triplet
  # nb_snp
  # reverse_nb_triplet
  # snp_processing

  # local variables
  flag_specif                = 0
  i                          = 0
  j                          = 0

  for i in range(nb_snp) :

    if snp_processing[i] in (1, 3) :

      # FORWARD
      # testing triplets of primers of SNP i in the forward direction
      orientation = "FORWARD"
      snp_path = "SNP_" + str(i + 1)
      current_working_directory_tmp = current_working_directory + dir_name + "/" + snp_path + "/" + orientation
      os.chdir(current_working_directory_tmp)

      flag_specif = 0
      j           = 1

      # lst_triplets_specificities = []  # specificity of each triplet of primers of the SNP i in the forward direction

      while j <= forward_nb_triplet[i] and forward_nb_triplet[i] > 0 :

        # testing the specificity of the triplet j for SNP i in the forward direction
        primers_triplet_specificity(i, j, orientation)
        flag_specif = 1
        j += 1

      # adding some informations about the specificity of the triplets of primers in file primers_triplets_sorted.csv
      if flag_specif :
        adding_specificities_informations()

    if snp_processing[i] in (2, 3) :
      # REVERSE
      # testing triplets of primers of SNP i in the reverse direction
      orientation = "REVERSE"
      snp_path = "SNP_" + str(i + 1)
      current_working_directory_tmp = current_working_directory + dir_name + "/" + snp_path + "/" + orientation
      os.chdir(current_working_directory_tmp)

      flag_specif = 0
      j           = 1

      # lst_triplets_specificities = []  # specificity of each triplet of primers of the SNP i in the reverse direction

      while j <= reverse_nb_triplet[i] and reverse_nb_triplet[i] > 0 :

        # testing the specificity of the triplet j for SNP i in the reverse direction
        primers_triplet_specificity(i, j, orientation)
        flag_specif = 1
        j += 1

      # adding some informations about the specificity of the triplets of primers in file primers_triplets_sorted.csv
      if flag_specif :
        adding_specificities_informations()

  # writing primers_triplets_specificities.csv file in the SNP directory
  writing_primers_triplets_specificities_file()



#=================================================================================#
# function testing_sequence(seq, seq_id) : testing the flanking sequence of a SNP #
#=================================================================================#

def testing_sequence(seq, seq_id) :

  if debug :
    print_msg("testing_sequence(" + seq + ", " + seq_id + ")")

  global snp_processing

  # local variables
  count_Ambi       = 0
  count_err        = 0
  count_N          = 0
  flag_forward     = 1
  flag_reverse     = 1
  flag_seq_id      = 0
  left_fl_seq      = ""
  message          = ""
  message_header   = "sequence " + str(seq_id) + " is rejected:\n"
  message_header_f = "sequence " + str(seq_id) + " is rejected for FORWARD:\n"
  message_header_r = "sequence " + str(seq_id) + " is rejected for REVERSE:\n"
  pos_0            = 0
  pos_1            = 0
  right_fl_seq     = ""
  snp              = ""
  test_seq         = ""

  # no '[' or ']' in the sequence
  if seq.count('[') == 0 or seq.count(']') == 0 :
    count_err = 1
    message = message_header + " - SNP must be defined with '[' and ']'\n"
    flag_seq_id = 1

  # unbalanced counts of '[' and of ']'
  if seq.count('[') != seq.count(']') :
    count_err += 1
    if not flag_seq_id :
      message = message_header
      flag_seq_id = 1
    message += " - unbalanced counts of '[' and of ']'\n"

  # more than one SNP are indicated in the sequence
  elif seq.count('[') > 1 :
    count_err += 1
    if not flag_seq_id :
      message = message_header
      flag_seq_id = 1
    message += " - only one SNP position indicated with '[' and ']' is allowed\n"

  # impossible to go further in this test
  if count_err :
    snp_processing.append(0)
    print_msg(message)
    return

  # only one [???] in seq
  pos_0 = seq.find('[')
  pos_1 = seq.find(']')
  snp   = seq[pos_0 + 1:pos_1]
  if not len(snp) :
    count_err = 1
    message = message_header
    message += " - the SNP is missing between '[' and ']'"    
  elif len(snp) == 1 :
    if snp not in ('R', 'Y', 'K', 'M', 'S','W') :
      count_err = 1
      message = message_header
      message += " - '" + snp + "' IUPAC code is not allowed to describe a SNP"
  elif len(snp) == 3 :
    if snp[0] not in ('A','C','G','T') or snp[1] != '/' or snp[2] not in ('A','C','G','T') :
      count_err = 1
      message = message_header
      message += " - '[" + snp + "]' doesn't describe correctly a SNP\n"
  else :
    # len(snp) not in (0, 1, 3)
    count_err = 1
    message = message_header
    message += " - '[" + snp + "]' doesn't describe correctly a SNP\n"

  # useless to go further in this test
  if count_err :
    snp_processing.append(0)
    print_msg(message)
    return
  
  # we have [A/T] or [W]. Now controlling the flanking sequences

  left_fl_seq  = seq[:pos_0]
  right_fl_seq = seq[pos_1 + 1:]
  both_fl_seq  = left_fl_seq + right_fl_seq

  if len(left_fl_seq) < flanking_sequence_min_size :
    count_err += 1
    if not flag_seq_id :
      message = message_header
      flag_seq_id = 1
    message += " - left flanking sequence's length is less than " + str(flanking_sequence_min_size) + "\n"

  if len(right_fl_seq) < flanking_sequence_min_size :
    count_err += 1
    if not flag_seq_id :
      message = message_header
      flag_seq_id = 1
    message += " - right flanking sequence's length is less than " + str(flanking_sequence_min_size) + "\n"

  # global count of N and of ambiguous codes
  count_N = both_fl_seq.count('N')

  if count_N > max_N_gl :
    count_err += 1
    if not flag_seq_id :
      message = message_header
      flag_seq_id = 1
    message += " - " + str(count_N) + " 'N' in the whole flanking sequence (maximum = " + str(max_N_gl) + ")\n"
    
  count_Ambi = both_fl_seq.count('R') + both_fl_seq.count('Y') + both_fl_seq.count('K') + \
      both_fl_seq.count('M') + both_fl_seq.count('S') + both_fl_seq.count('S') + \
      both_fl_seq.count('B') + both_fl_seq.count('D') + both_fl_seq.count('H') + both_fl_seq.count('V')

  if count_Ambi > max_Ambi_gl :
    count_err += 1
    if not flag_seq_id :
      message = message_header
      flag_seq_id = 1
    message += " - " + str(count_Ambi) + " ambiguous IUPAC in the whole flanking sequence (maximum = " + str(max_Ambi_gl) + ")\n"

  # useless to go further in this test
  if count_err :
    snp_processing.append(0)
    print_msg(message)
    return

  # 1. testing Forward
  # ------------------

  # SEQUENCE_FORCE_LEFT_END=60
  # SEQUENCE FAM TCCAACGACTCTCCCCTTCTCAAAATCGTTACCTGACCCATAACGATTCCTCCGTCCGCT G GCGTAGGCGTAACTTTGCCGGTGATTTGAACCGGTTTCGAAATCGACGGCTCAGATTGAT
  # SEQUENCE HEX TCCAACGACTCTCCCCTTCTCAAAATCGTTACCTGACCCATAACGATTCCTCCGTCCGCT T GCGTAGGCGTAACTTTGCCGGTGATTTGAACCGGTTTCGAAATCGACGGCTCAGATTGAT
  # PRIMER   FAM                                      CCATAACGATTCCTCCGTCCGCT T
  # PRIMER   HEX                                      CCATAACGATTCCTCCGTCCGCT G
  # rev cpl(PRIMER   COMMON)                                                                       GGTGATTTGAACCGGTTTCGAAATCGA
  # PRIMER   COMMON                                                                                TCGATTTCGAAACCGGTTCAAATCACC
  # tested sequences                                      AACGATTCCTCCGTCCGCT             AACTTTGCCGGTGATTTGAACCGGTTTCGAAATCGACGGCTCAGATTGA

  flag_forward = 1    # if rejected, -> 0
  flag_seq_id  = 0

  # local count of N and of ambiguous codes
  # 1.1. left flanking sequence : we verify for the minimum size of FAX and HEX
  # AACGATTCCTCCGTCCGCT (19) + SNP : 20
  test_seq = left_fl_seq[len(left_fl_seq) + 1 - primer_size_min:]

  count_N = test_seq.count('N')

  if count_N > max_N_loc :
    flag_forward = 0
    if not flag_seq_id :
      message = message_header_f
      flag_seq_id = 1
    message += " - " + str(count_N) + " 'N' in the last " + str(primer_size_max - 1) + " nt of the left flanking sequence (maximum = " + str(max_N_loc) + ")\n"

  count_Ambi = test_seq.count('R') + test_seq.count('Y') + test_seq.count('K') + \
      test_seq.count('M') + test_seq.count('S') + test_seq.count('S') + \
      test_seq.count('B') + test_seq.count('D') + test_seq.count('H') + test_seq.count('V')

  if count_Ambi > max_Ambi_loc :
    flag_forward = 0
    if not flag_seq_id :
      message = message_header_f
      flag_seq_id = 1
    message += " - " + str(count_Ambi) + " ambiguous IUPAC in the last " + str(primer_size_max - 1) + " nt of the left flanking sequence (maximum = " + str(max_Ambi_loc) + ")\n"

  if checked_zone_length > 0 :
    test_seq = test_seq[1 - checked_zone_length :]

    count_N = test_seq.count('N')

    if count_N > 0 :
      flag_forward = 0
      if not flag_seq_id :
        message = message_header_f
        flag_seq_id = 1
      message += " - " + str(count_N) + " 'N' in the last " + str(checked_zone_length - 1) + " nt of the left flanking sequence (none allowed)\n"

    count_Ambi = test_seq.count('R') + test_seq.count('Y') + test_seq.count('K') + \
        test_seq.count('M') + test_seq.count('S') + test_seq.count('S') + \
        test_seq.count('B') + test_seq.count('D') + test_seq.count('H') + test_seq.count('V')

    if count_Ambi > max_Ambi_loc :
      flag_forward = 0
      if not flag_seq_id :
        message = message_header_f
        flag_seq_id = 1
      message += " - " + str(count_Ambi) + " ambiguous IUPAC in the last " + str(checked_zone_length - 1) + " nt of the left flanking sequence (maximum = " + str(max_Ambi_loc) + ")\n"


  # 1.2. right flanking sequence
  # not so easy to define where to look ... as we have a large range of solutions according to the size of the 2 primers and of the PCR product
  # pos_0 and pos_1 as defined below could be a good compromise
  #  checked_zone_length has no sense on this side
  pos_0 = amp_size_min - primer_size_max - primer_size_min
  if pos_0 < 0 :
    pos_0 = 0
  pos_1 = amp_size_max - ((primer_size_max + primer_size_min) / 2) - 1
  if pos_1 < 0 :
    pos_1 = 0
  if pos_1 > len(right_fl_seq) :
    pos_1 = len(right_fl_seq)
  test_seq = right_fl_seq[pos_0:pos_1]

  count_N = test_seq.count('N')

  if count_N > max_N_loc :
    flag_forward = 0
    if not flag_seq_id :
      message = message_header_f
      flag_seq_id = 1
    message += " - " + str(count_N) + " 'N' in the interval [" + str(pos_0 + 1) + ", " + str(pos_1) + "] of the right flanking sequence (maximum = " + str(max_N_loc) + ")\n"

  count_Ambi = test_seq.count('R') + test_seq.count('Y') + test_seq.count('K') + \
      test_seq.count('M') + test_seq.count('S') + test_seq.count('S') + \
      test_seq.count('B') + test_seq.count('D') + test_seq.count('H') + test_seq.count('V')

  if count_Ambi > max_Ambi_loc :
    flag_forward = 0
    if not flag_seq_id :
      message = message_header_f
      flag_seq_id = 1
    message += " - " + str(count_Ambi) + " ambiguous IUPAC in the interval [" + str(pos_0 + 1) + ", " + str(pos_1) + "] of the right flanking sequence (maximum = " + str(max_Ambi_loc) + ")\n"

  if not flag_forward :
    print_msg(message)

  # 2. testing Reverse
  # ------------------

  # SEQUENCE_FORCE_RIGHT_END=60

  # SEQUENCE FAM      TCCAACGACTCTCCCCTTCTCAAAATCGTTACCTGACCCATAACGATTCCTCCGTCCGCT T GCGTAGGCGTAACTTTGCCGGTGATTTGAACCGGTTTCGAAATCGACGGCTCAGATTGAT
  # SEQUENCE HEX      TCCAACGACTCTCCCCTTCTCAAAATCGTTACCTGACCCATAACGATTCCTCCGTCCGCT G GCGTAGGCGTAACTTTGCCGGTGATTTGAACCGGTTTCGAAATCGACGGCTCAGATTGAT
  # rev cpl(PRIMER   FAM)                                                          T GCGTAGGCGTAACTTTGCCGG
  # rev cpl(PRIMER   HEX)                                                          G GCGTAGGCGTAACTTTGCCGG
  # PRIMER   FAM                                                                    CCGGCAAAGTTACGCCTACGCA
  # PRIMER   HEX                                                                    CCGGCAAAGTTACGCCTACGCC
  # PRIMER   COMMON              TCCCCTTCTCAAAATCGTTACCTGACCC
  # tested sequences    CAACGACTCTCCCCTTCTCAAAATCGTTACCTGACCCATAACGATTCC                                                       GCGTAGGCGTAACTTTGCC
  # checked_zone_length                                                              GCGTAGGCG

  flag_reverse = 1    # if rejected, -> 0
  flag_seq_id  = 0

  # local count of N and of ambiguous codes
  # 2.1. right flanking sequence : we verify for the minimum size of FAX and HEX
  # SNP + GCGTAGGCGTAACTTTGCCG (19) : 20
  test_seq = right_fl_seq[:primer_size_min - 1]

  count_N = test_seq.count('N')

  if count_N > max_N_loc :
    flag_reverse = 0
    if not flag_seq_id :
      message = message_header_r
      flag_seq_id = 1
    message += " - " + str(count_N) + " 'N' in the first " + str(primer_size_max - 1) + " nt of the right flanking sequence (maximum = " + str(max_N_loc) + ")\n"

  count_Ambi = test_seq.count('R') + test_seq.count('Y') + test_seq.count('K') + \
      test_seq.count('M') + test_seq.count('S') + test_seq.count('S') + \
      test_seq.count('B') + test_seq.count('D') + test_seq.count('H') + test_seq.count('V')

  if count_Ambi > max_Ambi_loc :
    flag_reverse = 0
    if not flag_seq_id :
      message = message_header_r
      flag_seq_id = 1
    message += " - " + str(count_Ambi) + " ambiguous IUPAC in the first " + str(primer_size_max - 1) + " nt of the right flanking sequence (maximum = " + str(max_Ambi_loc) + ")\n"

  if checked_zone_length > 0 :
    test_seq = test_seq[:checked_zone_length - 1]

    count_N = test_seq.count('N')

    if count_N > 0 :
      flag_reverse = 0
      if not flag_seq_id :
        message = message_header_r
        flag_seq_id = 1
      message += " - " + str(count_N) + " 'N' in the first " + str(checked_zone_length - 1) + " nt of the right flanking sequence (none allowed)\n"

    count_Ambi = test_seq.count('R') + test_seq.count('Y') + test_seq.count('K') + \
        test_seq.count('M') + test_seq.count('S') + test_seq.count('S') + \
        test_seq.count('B') + test_seq.count('D') + test_seq.count('H') + test_seq.count('V')

    if count_Ambi > max_Ambi_loc :
      flag_reverse = 0
      if not flag_seq_id :
        message = message_header_r
        flag_seq_id = 1
      message += " - " + str(count_Ambi) + " ambiguous IUPAC in the first " + str(checked_zone_length - 1) + " nt of the right flanking sequence (maximum = " + str(max_Ambi_loc) + ")\n"

  # 2.2. left flanking sequence
  # not so easy to define where to look ... as we have a large range of solutions according to the size of the 2 primers and of the PCR product
  # pos_0 and pos_1 as defined below could be a good compromise
  #  checked_zone_length has no sense on this side
  pos_0 = amp_size_max - ((primer_size_max + primer_size_min) / 2) - 1
  if pos_0 < 0 :
    pos_0 = 0
  if pos_0 > len(left_fl_seq) :
    pos_0 = len(left_fl_seq)
  pos_1 = amp_size_min - primer_size_max - primer_size_min
  if pos_1 < 0 :
    pos_1 = 0
  if pos_1 > len(left_fl_seq) :
    pos_1 = len(left_fl_seq)
  test_seq = left_fl_seq[-pos_0:-pos_1]

  count_N = test_seq.count('N')

  if count_N > max_N_loc :
    flag_reverse = 0
    if not flag_seq_id :
      message = message_header_r
      flag_seq_id = 1
    message += " - " + str(count_N) + " 'N' in the interval [" + str(len(left_fl_seq) - pos_0 + 1) + ", " + str(len(left_fl_seq) - pos_1) + "] of the left flanking sequence (maximum = " + str(max_N_loc) + ")\n"

  count_Ambi = test_seq.count('R') + test_seq.count('Y') + test_seq.count('K') + \
      test_seq.count('M') + test_seq.count('S') + test_seq.count('S') + \
      test_seq.count('B') + test_seq.count('D') + test_seq.count('H') + test_seq.count('V')

  if count_Ambi > max_Ambi_loc :
    flag_reverse = 0
    if not flag_seq_id :
      message = message_header_r
      flag_seq_id = 1
    message += " - " + str(count_Ambi) + " ambiguous IUPAC in the interval [" + str(len(left_fl_seq) - pos_0 + 1) + ", " + str(len(left_fl_seq) - pos_1) + "] of the left flanking sequence (maximum = " + str(max_Ambi_loc) + ")\n"


  if not flag_reverse :
    print_msg(message)

  if flag_forward :
    if flag_reverse :
      # both flanking sequences are right
      snp_processing.append(3)
    else :
      # only FORWARD is OK
      snp_processing.append(1)
  elif flag_reverse :
    # only REVERSE is OK
    snp_processing.append(2)
  else :
    snp_processing.append(0)



#=======================================================================================================#
# function writing_all_primers_triplets_files() : to write files containing all the primers of triplets #
#=======================================================================================================#

def writing_all_primers_triplets_files() :

  if debug :
    print_msg("writing_all_primers_triplets_files()")

  # writing a file containing all the triplets of primers of the forward alleles
  # sorted by id; FORWARD|REVERSE
  # we don't keep files forward_primers_triplets.csv and reverse_primers_triplets.csv created in version 0.1
  # jpb - 16/06/2014

  # global variables
  global current_working_directory_tmp, orientation

  # global unmodified variables
  # current_working_directory
  # dir_name
  # lst_snp_id
  # snp_processing
  # species_db_name

  # local variables
  dct_forward = {}
  dct_reverse = {}
  fl_tmp      = 0
  flag_found  = 0
  flag_header = 0
  i           = 0
  j           = 0
  id_snp      = ""
  line        = ""
  line_header = ""
  lst_field   = []
  snp_path    = ""
  no_line     = 0

  # FORWARD TRIPLETS
  orientation = "FORWARD"
  
  for i in range(nb_snp) :
    snp_path = "SNP_" + str(i + 1)

    if snp_processing[i] in (1, 3) :
      # the SNP is processed
      current_working_directory_tmp = current_working_directory + dir_name + "/" + snp_path + "/" + orientation
      os.chdir(current_working_directory_tmp)

      try:
        fl_tmp  = open("primers_triplets_sorted.csv", "r")
      except :
        fl_tmp = 0
        error(14, current_working_directory_tmp + "/primers_triplets_sorted.csv")

      if fl_tmp :
        no_line = 0

        while 1 :

          line = fl_tmp.readline()

          if line == "" :
            # EOF ...
            break

          no_line += 1
          line = chop(line)
          line = line.strip()

          if not len(line) :
            continue

          if no_line == 1 :
            if not flag_header :
              line_header = line
              flag_header = 1
            
          else :
            # no_line > 1
            lst_field = line.split(";")
            if len(lst_field) > 0 :
              id_snp = lst_field[0]
              if id_snp not in dct_forward:
                dct_forward[id_snp] = list()
              dct_forward[id_snp].append(line)

        fl_tmp.close()

  # REVERSE TRIPLETS
  orientation = "REVERSE"

  for i in range(nb_snp) :
    snp_path =  "SNP_" + str(i + 1)

    if snp_processing[i] in (2, 3) :
      # the SNP is processed
      current_working_directory_tmp = current_working_directory + dir_name + "/" + snp_path + "/" + orientation
      os.chdir(current_working_directory_tmp)

      try :
        fl_tmp  = open("primers_triplets_sorted.csv", "r")
      except :
        fl_tmp = 0
        error(14, current_working_directory + dir_name + "/SNP_" + str(i) + "/REVERSE/primers_triplets_sorted.csv")

      if fl_tmp :
        no_line = 0

        while 1 :

          line = fl_tmp.readline()

          if line == "" :
            # EOF ...
            break

          no_line += 1
          line = chop(line)
          line = line.strip()

          if not len(line) :
            continue

          if no_line == 1 :
            if not flag_header :
              line_header = line
              flag_header = 1
            
          else :
            # no_line > 1
            lst_field = line.split(";")
            if len(lst_field) > 0 :
              id_snp = lst_field[0]
              if id_snp not in dct_reverse:
                dct_reverse[id_snp] = list()
              dct_reverse[id_snp].append(line)

        fl_tmp.close()

  # writing a file containing all the triplets of primers for the forward and reverse alleles

  current_working_directory_tmp = current_working_directory + dir_name
  os.chdir(current_working_directory_tmp)

  try :
    fl_forward_reverse_primers_triplets = open("all_primers_triplets.csv",'w')
  except :
    error(7, current_working_directory + dir_name + "/all_primers_triplets.csv")

  flag_header = 0
  for i in range(len(lst_snp_id)) :
    id_snp     = lst_snp_id[i]
    flag_found = 0
    if id_snp in dct_forward :
      flag_found = 1
      for j in range(len(dct_forward[id_snp])) :
        if not flag_header :
          fl_forward_reverse_primers_triplets.write(line_header + "\n")
          flag_header = 1

        fl_forward_reverse_primers_triplets.write(dct_forward[id_snp][j] + "\n")

    if id_snp in dct_reverse :
      flag_found = 1
      for j in range(len(dct_reverse[id_snp])) :
        if not flag_header :
          fl_forward_reverse_primers_triplets.write(line_header + "\n")
          flag_header = 1

        fl_forward_reverse_primers_triplets.write(dct_reverse[id_snp][j] + "\n")

    if not flag_found :
      if not flag_header :
        fl_forward_reverse_primers_triplets.write(line_header + "\n")
        flag_header = 1

      if len(species_db_name) :
        fl_forward_reverse_primers_triplets.write(id_snp + ";both;none;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n")
      else :
        fl_forward_reverse_primers_triplets.write(id_snp + ";both;none;;;;;;;;;;;;;;;;;;;;;;;;;\n")

  # that's all
  fl_forward_reverse_primers_triplets.close()



#==========================================================================#
# function writing_primer3_input_files() : writing the Primer3 input files #
#==========================================================================#

def writing_primer3_input_files() :

  if debug :
    print_msg("writing_primer3_input_files()")

  # global variables
  global amp_size_max, amp_size_min, amp_size_opt, lst_snp_id, no_snp, orientation, primer_size_max, primer_size_min

  # local variables
  dct_pcr_parameters   = {}
  fl_allele_fam        = 0
  fl_allele_hex        = 0
  fl_pcr_parameters    = 0
  i                    = 0
  line                 = ""
  lst_field            = []
  lst_field_02         = []
  pcr_parameters       = ""
  seq                  = ""
  sequence_template_01 = ""
  sequence_template_02 = ""
  snp_id               = ""
  snp_position         = 0
  value_0              = ""
  value_1              = ""

  # first step: defining PCR parameters that are not specific of each SNP
  # 1. default PCR parameters file
  try :
    fl_pcr_parameters = open(fl_pcr_parameters_default_name, 'r')
  except :
    error(6, fl_pcr_parameters_default_name)

  while 1 :
    line = fl_pcr_parameters.readline()

    if line == "" :
      # EOF ...
      break
    
    line = chop(line)
    if not len(line) :
      continue

    lst_field = line.split("=")
    if len(lst_field) != 2 :
      error(10, "(default file)")
    
    value_0 = lst_field[0].strip()
    value_1 = lst_field[1].strip()

    dct_pcr_parameters[value_0] = value_1

    if value_0 == "PRIMER_PRODUCT_SIZE_RANGE" :
      amp_size_min = int(value_1[:value_1.find("-")])
      amp_size_max = int(value_1[value_1.find("-") + 1 :])
    elif value_0 == "PRIMER_PRODUCT_OPT_SIZE" :
      amp_size_opt = int(value_1)

  fl_pcr_parameters.close()

  # 2. user PCR parameters file
  if len(fl_pcr_parameters_name) :
    try :
      fl_pcr_parameters = open(fl_pcr_parameters_name, 'r')
    except :
      error(6, fl_pcr_parameters_name)

    while 1 :
      line = fl_pcr_parameters.readline()

      if line == "" :
        # EOF ...
        break
    
      line = chop(line)
      if not len(line) :
        continue

      lst_field = line.split("=")
      if len(lst_field) != 2 :
        error(10, "(PCR parameters user file)")
    
      value_0 = lst_field[0].strip()
      value_1 = lst_field[1].strip()

      dct_pcr_parameters[value_0] = value_1

      if value_0 == "PRIMER_PRODUCT_SIZE_RANGE" :
        amp_size_min = int(value_1[:value_1.find("-")])
        #print(amp_size_min)
        amp_size_max = int(value_1[value_1.find("-") + 1 :])
      elif value_0 == "PRIMER_PRODUCT_OPT_SIZE" :
        amp_size_opt = int(value_1)

    fl_pcr_parameters.close()
    
  # 3. user PCR parameters given as arguments
  for i in range(len(lst_pcr_parameters)) :
    lst_field = lst_pcr_parameters[i].split("=")
    if len(lst_field) != 2 :
      error(10, "(PCR parameters arguments)")

    value_0 = lst_field[0].strip()
    value_1 = lst_field[1].strip()

    dct_pcr_parameters[value_0] = value_1

    if value_0 == "PRIMER_PRODUCT_SIZE_RANGE" :
      amp_size_min = int(value_1[:value_1.find("-")])
      amp_size_max = int(value_1[value_1.find("-") + 1 :])
    elif value_0 == "PRIMER_PRODUCT_OPT_SIZE" :
      amp_size_opt = int(value_1)

  # 4. -max_primer n3
  if max_primer != -1 :
    dct_pcr_parameters["PRIMER_NUM_RETURN"] = max_primer_s

  # primer_size_max and primer_size_min
  if "PRIMER_MAX_SIZE" in dct_pcr_parameters :
    primer_size_max = int(dct_pcr_parameters["PRIMER_MAX_SIZE"])
  else :
    primer_size_max = primer_size_max_dft
  if "PRIMER_MIN_SIZE" in dct_pcr_parameters :
    primer_size_min = int(dct_pcr_parameters["PRIMER_MIN_SIZE"])
  else :
    primer_size_min = primer_size_min_dft

  # set of PCR parameters
  lst_field = dct_pcr_parameters.keys()
  lst_field.sort()
  pcr_parameters = ""
  for i in range(len(lst_field)) :
    pcr_parameters +=  lst_field[i] + "=" + dct_pcr_parameters[lst_field[i]] + "\n"

  # second step: writing Primer3 input file for each SNP
  while 1 :
    line = fl_input.readline()

    if line == "" :
      # EOF ...
      break

    line = chop(line)

    if not len(line) :
      continue

    if line.find(">") == 0 :
      #print(line)
      no_snp += 1

      lst_field = line.split('>')
      snp_id    = lst_field[1]
      lst_snp_id.append(snp_id)

      try:
        os.mkdir(current_working_directory + dir_name + "/SNP_" + str(no_snp))
      except OSError:
        pass

      try:
        os.mkdir(current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/FORWARD")
      except OSError:
        pass

      try:
        os.mkdir(current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/REVERSE")
      except OSError:
        pass

    else :
      # sequence line (still in only one line)

      # testing the input sequence
      line = line.upper()
      testing_sequence(line, snp_id)

      #print(snp_processing,len(snp_processing),no_snp)
      if snp_processing[no_snp - 1] > 0 :
        # the SNP has been tested

        lst_field = line.split('[')
        lst_field_02 = lst_field[1].split(']')

        snp_position = len(lst_field[0])

        if lst_field_02[0].find('/') != -1 :
          lst_field_03 = lst_field_02[0].split("/")
          sequence_template_01 = lst_field[0] + lst_field_03[0] + lst_field_02[1]
          sequence_template_02 = lst_field[0] + lst_field_03[-1] + lst_field_02[1]
        elif lst_field_02[0] == 'R' :
          sequence_template_01 = lst_field[0] + 'A' + lst_field_02[1]
          sequence_template_02 = lst_field[0] + 'G' + lst_field_02[1]
        elif lst_field_02[0] == 'Y' :
          sequence_template_01 = lst_field[0] + 'C' + lst_field_02[1]
          sequence_template_02 = lst_field[0] + 'T' + lst_field_02[1]
        elif lst_field_02[0] == 'S' :
          sequence_template_01 = lst_field[0] + 'G' + lst_field_02[1]
          sequence_template_02 = lst_field[0] + 'C' + lst_field_02[1]
        elif lst_field_02[0] == 'W' :
          sequence_template_01 = lst_field[0] + 'A' + lst_field_02[1]
          sequence_template_02 = lst_field[0] + 'T' + lst_field_02[1]
        elif lst_field_02[0] == 'K' :
          sequence_template_01 = lst_field[0] + 'G' + lst_field_02[1]
          sequence_template_02 = lst_field[0] + 'T' + lst_field_02[1]
        elif lst_field_02[0] == 'M' :
          sequence_template_01 = lst_field[0] + 'A' + lst_field_02[1]
          sequence_template_02 = lst_field[0] + 'C' + lst_field_02[1]
        else :
          error(11, str(no_snp) + " (" + line + ")")
          continue

        if snp_processing[no_snp - 1] in (1, 3) :
          orientation = "FORWARD"
          os.chdir(current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/" + orientation)
          current_working_directory_tmp = current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/" + orientation

          try :
            fl_allele_fam = open("fam_primer3_input.txt", 'w')
          except :
            error(7, current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/" + orientation + "/fam_primer3_input.txt")

          try :
            fl_allele_hex = open("hex_primer3_input.txt", 'w')
          except :
            error(7, current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/" + orientation + "/hex_primer3_input.txt")

          fl_allele_fam.write("SEQUENCE_ID=" + snp_id + "\n")
          fl_allele_fam.write("SEQUENCE_TEMPLATE=" + sequence_template_01 + "\n")
          fl_allele_fam.write("SEQUENCE_FORCE_LEFT_END=" + str(snp_position) + "\n")
          fl_allele_fam.write(pcr_parameters)
          fl_allele_fam.write("=\n")

          fl_allele_hex.write("SEQUENCE_ID=" + snp_id + "\n")
          fl_allele_hex.write("SEQUENCE_TEMPLATE=" + sequence_template_02 + "\n")
          fl_allele_hex.write("SEQUENCE_FORCE_LEFT_END=" + str(snp_position) + "\n")
          fl_allele_hex.write(pcr_parameters)
          fl_allele_hex.write("=\n")
        
          fl_allele_fam.close()
          fl_allele_hex.close()

        if snp_processing[no_snp - 1] in (2, 3) :
          orientation = "REVERSE"
          os.chdir(current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/" + orientation)
          current_working_directory_tmp = current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/" + orientation

          try :
            fl_allele_fam = open("fam_primer3_input.txt", 'w')
          except :
            error(7, current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/" + orientation + "/fam_primer3_input.txt")

          try :
            fl_allele_hex = open("hex_primer3_input.txt", 'w')
          except :
            error(7, current_working_directory + dir_name + "/SNP_" + str(no_snp) + "/" + orientation + "/hex_primer3_input.txt")

          fl_allele_fam.write("SEQUENCE_ID=" + snp_id + "\n")
          fl_allele_fam.write("SEQUENCE_TEMPLATE=" + sequence_template_01 + "\n")
          fl_allele_fam.write("SEQUENCE_FORCE_RIGHT_END=" + str(snp_position) + "\n")
          fl_allele_fam.write(pcr_parameters)
          fl_allele_fam.write("=\n")

          fl_allele_hex.write("SEQUENCE_ID=" + snp_id + "\n")
          fl_allele_hex.write("SEQUENCE_TEMPLATE=" + sequence_template_02 + "\n")
          fl_allele_hex.write("SEQUENCE_FORCE_RIGHT_END=" + str(snp_position) + "\n")
          fl_allele_hex.write(pcr_parameters)
          fl_allele_hex.write("=\n")

          fl_allele_fam.close()
          fl_allele_hex.close()



#========================================================================================================================================================#
# function writing_primers_triplets_specificities_file() : to write file SNP/primers_triplets_specificities.csv including all specificities informations #
#========================================================================================================================================================#

def writing_primers_triplets_specificities_file() :

  if debug :
    print_msg("writing_primers_triplets_specificities_file()")

  # compilation of all files triplet_i_best_ampli.csv in file primers_triplets_specificities.csv
  # updating of file nb_triplets_by_snp.csv

  global current_working_directory_tmp, orientation

  # global unmodified variables
  # forward_nb_triplet_amp
  # snp_processing
  # reverse_nb_triplet_amp

  # local variables
  fl_nb_triplets_by_snp             = 0    # file nb_triplets_by_snp.csv
  fl_nb_triplets_by_snp_tmp         = 0    # temporary file nb_triplets_by_snp_tmp.csv
  fl_primers_triplets_specificities = 0    # file primers_triplets_specificities.csv
  fl_triplet_specificity            = 0    # all files triplet_i_best_ampli.csv
  flag_file_specif                  = 0
  i                                 = 0
  j                                 = 0
  line                              = ""
  no_line                           = 0
  no_snp                            = 0

  os.chdir(current_working_directory + dir_name)
  current_working_directory_tmp = current_working_directory + dir_name

  try :
    fl_primers_triplets_specificities = open("primers_triplets_specificities.csv",'w')
  except :
    error(7, current_working_directory_tmp + "/primers_triplets_specificities.csv")

  i = 1

  # forward direction
  while i <= nb_snp :

    # the SNP is processed
    if snp_processing[i - 1] in (1, 3) :

      # 1. FORWARD
      os.chdir(current_working_directory + dir_name + "/SNP_" + str(i) + "/FORWARD")
      current_working_directory_tmp = current_working_directory + dir_name + "/SNP_" + str(i) + "/FORWARD"

      j = 1

      while j <= forward_nb_triplet[i - 1] and forward_nb_triplet[i - 1] > 0 :

        try :
          fl_triplet_specificity = open("triplet_" + str(j) + "_best_ampli.csv",'r')
        except :
          # Should not occur tkanks to forward_nb_triplet
          continue

        # fl_triplet_specificity.readline()
        line_no = 0
        while 1 :
          line = fl_triplet_specificity.readline()

          if line == "" :
            # EOF ...
            break

          line_no += 1
          line     = chop(line)
          line     = line.strip()
          if not len(line) :
            continue

          if len(line) and line_no == 1 :
            # skipping first line (headers)
            continue

          # line to copy

          if not flag_file_specif :
            fl_primers_triplets_specificities.write("id;orientation;triplet;" + \
                                                      "scaffold;start on scaffold;SNP position;end on scaffold;scaffold length;size of amplicon;class for amplification;distribution for amplification;comments;" + \
                                                      "sequence of common;start of common;end of common;length of common;orientation of common;identity for common;HSP class for common;distribution for common;primer antiscore for common;" + \
                                                      "sequence of FAM;start of FAM;end of FAM;length of FAM;orientation of FAM;identity for FAM;HSP class for FAM;distribution for FAM;primer antiscore for FAM;" + \
                                                      "sequence of HEX;start of HEX;end of HEX;length of HEX;orientation of HEX;identity for HEX;HSP class for HEX;distribution for HEX;primer antiscore for HEX\n")
            flag_file_specif = 1

          fl_primers_triplets_specificities.write(line + "\n")

        # all interesting lines have been copied
        fl_triplet_specificity.close()

        j += 1

      # end of FORWARD files for SNP_i

    if snp_processing[i - 1] in (2, 3) :
      # 2. REVERSE
      os.chdir(current_working_directory + dir_name + "/SNP_" + str(i) + "/REVERSE")
      current_working_directory_tmp = current_working_directory + dir_name + "/SNP_" + str(i) + "/REVERSE"

      j = 1

      while j <= reverse_nb_triplet[i - 1] and reverse_nb_triplet[i - 1] > 0 :

        try :
          fl_triplet_specificity = open("triplet_" + str(j) + "_best_ampli.csv",'r')
        except :
          # Should not occur tkanks to reverse_nb_triplet
          continue

        # fl_triplet_specificity.readline()
        line_no = 0
        while 1 :
          line = fl_triplet_specificity.readline()

          if line == "" :
            # EOF ...
            break

          line_no += 1
          line     = chop(line)
          line     = line.strip()
          if not len(line) :
            continue

          if len(line) and line_no == 1 :
            # skipping first line (headers)
            continue

          # line to copy

          if not flag_file_specif :
            fl_primers_triplets_specificities.write("id;orientation;triplet;" + \
                                                      "scaffold;start on scaffold;SNP position;end on scaffold;scaffold length;size of amplicon;class for amplification;distribution for amplification;comments;" + \
                                                      "sequence of common;start of common;end of common;length of common;orientation of common;identity for common;HSP class for common;distribution for common;primer antiscore for common;" + \
                                                      "sequence of FAM;start of FAM;end of FAM;length of FAM;orientation of FAM;identity for FAM;HSP class for FAM;distribution for FAM;primer antiscore for FAM;" + \
                                                      "sequence of HEX;start of HEX;end of HEX;length of HEX;orientation of HEX;identity for HEX;HSP class for HEX;distribution for HEX;primer antiscore for HEX\n")
            flag_file_specif = 1

          fl_primers_triplets_specificities.write(line + "\n")

        # all interesting lines have been copied
        fl_triplet_specificity.close()

        j += 1

      # end of REVERSE files for SNP_i

    # if snp_processing[i - 1] in (1, 3) 
    # if snp_processing[i - 1] in (2, 3) 
    i += 1

  # All SNP have been retrieved
  fl_primers_triplets_specificities.close()

  # updating file nb_triplets_by_snp.csv
  os.chdir(current_working_directory + dir_name)
  current_working_directory_tmp = current_working_directory + dir_name

  os.rename("nb_triplets_by_snp.csv", "nb_triplets_by_snp_tmp.csv")
  try :
    fl_nb_triplets_by_snp = open("nb_triplets_by_snp.csv",'w')
  except :
    error(7, current_working_directory_tmp + "/nb_triplets_by_snp.csv")

  try :
    fl_nb_triplets_by_snp_tmp = open("nb_triplets_by_snp_tmp.csv",'r')
  except :
    error(6, current_working_directory_tmp + "/nb_triplets_by_snp_tmp.csv")

  flag_file_specif = 0
  line_no          = 0
  no_snp           = 0

  while 1 :
    line = fl_nb_triplets_by_snp_tmp.readline()

    if line == "" :
      # EOF ...
      break
    
    line_no += 1
    line     = chop(line)
    if not len(line) :
      continue
    
    if len(line) and line_no == 1 :
      # skipping first line (headers)
      continue

    # data line
    no_snp += 1
    if not flag_file_specif :
      fl_nb_triplets_by_snp.write("id;folder;total of triplets;forward;reverse;total with amplification;forward with amplification;reverse with amplification\n")
      flag_file_specif = 1

    fl_nb_triplets_by_snp.write(line + ";" + str(forward_nb_triplet_amp[no_snp - 1] + reverse_nb_triplet_amp[no_snp - 1]) + ";"  + str(forward_nb_triplet_amp[no_snp - 1]) + ";" + str(reverse_nb_triplet_amp[no_snp - 1]) + "\n")
    
  fl_nb_triplets_by_snp.close()
  fl_nb_triplets_by_snp_tmp.close()
  os.remove("nb_triplets_by_snp_tmp.csv")



#===========================================================================#
# Main part of this program                                                 #
#===========================================================================#

if __name__ == "__main__" :

  # Defining global variables
  #==========================

  amp_size_max                   = 0                   # PRIMER_PRODUCT_SIZE maximum
  amp_size_max_dft               = 85
  amp_size_min                   = 0                   # PRIMER_PRODUCT_SIZE minimum
  amp_size_min_dft               = 62
  amp_size_opt                   = 0                   # PRIMER_PRODUCT_SIZE optimum
  amp_size_opt_dft               = 75
  amp_size_limit_1               = 0                   # initialization in main after primers_triplets_design()
  amp_size_limit_2               = 0                   # idem
  checked_zone_length            = 10                  # no ambiguous IUPAC code among the 10 nucleotides before and after the SNP in a sequence
  checked_zone_length_s          = ""                  # 
  count_SNP_eliminated           = 0
  count_SNP_processed            = 0
  count_SNP_successful           = 0
  count_triplets_forward         = 0
  count_triplets_reverse         = 0
  current_working_directory      = os.getcwd() + "/"   # working directory from where this program has been launched
  current_working_directory_tmp  = ""                  # current working directory
  debug                          = 0                   # debug mode [0|1]
  dir_name                       = ""                  # path of the directory used for the run
  dir_name_dft                   = "SNP"
  fl_err                         = 0                   # error file (-err parameter)
  fl_err_name                    = ""
  fl_input                       = 0                   # file containing a list of SNP with their backward and forward sequences (-in parameter)
  fl_input_name                  = ""
  fl_pcr_parameters_default      = 0                   # file containing the default PCR parameters
  fl_pcr_parameters_default_name = "/pcr_parameters_default.txt"
  fl_pcr_parameters              = 0                   # file containing the PCR parameters
  fl_pcr_parameters_name         = ""
  flanking_sequence_min_size     = 50                  # minimum length of the flanking sequences
  forward_nb_triplet             = []                  # list indicating the number of triplets of primers for each SNP in the forward direction
  forward_nb_triplet_amp         = []                  # list indicating the number of triplets of primers with expected amplification for each SNP in the forward direction
  lst_parameters                 = ()                  # list of parameters
  lst_pcr_parameters             = []                  # list of PCR parameters given as arguments
  lst_snp_id                     = []                  # list containing the SNPs identifiers
  max_Ambi_gl                    = 0
  max_Ambi_gl_s                  = ""
  max_Ambi_loc                   = 0
  max_Ambi_loc_s                 = ""
  max_N_gl                       = 0
  max_N_gl_s                     = ""
  max_N_loc                      = 0
  max_N_loc_s                    = ""
  max_primer                     = -1
  max_primer_s                   = ""
  max_triplet                    = 0
  max_triplet_dft                = 5
  max_triplet_s                  = ""
  message                        = ""
  nb_arg                         = len(sys.argv)       # count of arguments
  nb_snp                         = 0                   # number of SNPs
  nb_triplet                     = 0                   # number of triplets
  no_line                        = 0                   # current no of line
  no_snp                         = 0                   # current no of SNP
  orientation                    = ""                  # FORWARD|REVERSE
  path_blastall                  = "blastall"
  path_filtre_blastall           = "python /filtre_blastall_ncbi"
  path_primer3                   = "/primer/src/primer3_core"
  primer_size_max                = 0                   # PRIMER_MAX_SIZE
  primer_size_max_dft            = 32                  # PRIMER_MAX_SIZE
  primer_size_min                = 0                   # PRIMER_MIN_SIZE
  primer_size_min_dft            = 20                  # PRIMER_MIN_SIZE
  reverse_nb_triplet             = []                  # list indicating the number of triplets of primers for each SNP in the reverse direction
  reverse_nb_triplet_amp         = []                  # list indicating the number of triplets of primers with expected amplification for each SNP in the reverse direction
  snp_processing                 = []                  # list indicating if a SNP is processed or not(0 = eliminated, 1 = !forward, 2 = !reverse, 3 = both)
  species_db_name                = ""                  # path to the data base of the species used to check the specificities of the triplets of primers

  # Processing  arguments
  #======================

  if (nb_arg < 3 or nb_arg > 38) and (nb_arg != 2 or sys.argv[1] not in ("-h", "-H", "-pcr_parameters_default")) :
    error(3)

  lst_parameters = ("-h", "-H", "-pcr_parameters_default", "-in", "-folder", "-flanking_sequence_min_size", "-checked_zone_length", "-pcr_parameters_file", "-species_db", "-max_primer", "-max_triplet", "-max_N_global", "-max_N_local", "-max_Ambi_global", "-max_Ambi_local", "-err", "debug")

  i = 1
  while i < nb_arg :
    if sys.argv[i] == "-h" :
      help()
    elif sys.argv[i] == "-H" :
      help_detailed()
    elif sys.argv[i] == "-pcr_parameters_default" :
      getting_default_pcr_parameters()
    elif sys.argv[i] == "-in" and i < nb_arg :
      fl_input_name = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-folder" and i < nb_arg :
      dir_name = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-flanking_sequence_min_size" and i < nb_arg :
      flanking_sequence_min_size = int(sys.argv[i + 1])
      i += 1
    elif sys.argv[i] == "-checked_zone_length" and i < nb_arg :
      checked_zone_length_s = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-pcr_parameters_file" and i < nb_arg :
      fl_pcr_parameters_name = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-pcr_parameters" and i < nb_arg :
      i += 1
      while sys.argv[i] not in lst_parameters :
        lst_pcr_parameters.append(sys.argv[i])
        if i + 1 == nb_arg or sys.argv[i + 1]in lst_parameters :
          break
        else :
          i += 1
    elif sys.argv[i] == "-species_db" and i < nb_arg :
      species_db_name = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-max_primer" and i < nb_arg :
      max_primer_s = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-max_triplet" and i < nb_arg :
      max_triplet_s = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-max_N_global" and i < nb_arg :
      max_N_gl_s = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-max_N_local" and i < nb_arg :
      max_N_loc_s = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-max_Ambi_global" and i < nb_arg :
      max_Ambi_gl_s = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-max_Ambi_local" and i < nb_arg :
      max_Ambi_loc_s = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-err" and i < nb_arg :
      fl_err_name = sys.argv[i + 1]
      i += 1
    elif sys.argv[i] == "-debug" :
      debug = 1
    else :
      error(3)
    i += 1

  # Checking arguments and opening files
  # ====================================

  # fl_input_name : file to parse
  if len(fl_input_name) :
    try :
      fl_input = open(fl_input_name, 'r')
    except :
      error(6, fl_input_name)
  else :
      error(3, "-in")

  # folder
  if not len(dir_name) :
    dir_name = dir_name_dft

  # errors file
  if len(fl_err_name) :
    try :
      fl_err = open(fl_err_name, 'w')
    except :
      error(7, fl_err_name)

  # -checked_zone_length l2
  if len(checked_zone_length_s) :
    try :
      checked_zone_length = int(checked_zone_length_s)
    except :
      error(13, "-checked_zone_length" + " (" + checked_zone_length_s + ")")
    if checked_zone_length < 0 :
      error(13, "-checked_zone_length" + " (" + checked_zone_length_s + ")")

  # -max_primer n3
  if len(max_primer_s) :
    try :
      max_primer = int(max_primer_s)
    except :
      error(13, "-max_primer" + " (" + max_primer_s + ")")
    if max_primer < 0 :
      error(13, "-max_primer" + " (" + max_primer_s + ")")

  # -max_triplet n4
  if len(max_triplet_s) :
    try :
      max_triplet = int(max_triplet_s)
    except :
      error(13, "-max_triplet" + " (" + max_triplet_s + ")")
    if max_triplet < 1 :
      error(13, "-max_triplet" + " (" + max_triplet_s + ")")
  else :
    max_triplet = max_triplet_dft

  # -max_N_global n5
  if len(max_N_gl_s) :
    try :
      max_N_gl = int(max_N_gl_s)
    except :
      error(13, "-max_N_global" + " (" + max_N_gl_s + ")")
    if max_N_gl < 0 :
      error(13, "-max_N_global" + " (" + max_N_gl_s + ")")

  # -max_N_local n6
  if len(max_N_loc_s) :
    try :
      max_N_loc = int(max_N_loc_s)
    except :
      error(13, "-max_N_local" + " (" + max_N_loc_s + ")")
    if max_N_loc < 0 :
      error(13, "-max_N_local" + " (" + max_N_loc_s + ")")

  # -max_Ambi_global n7
  if len(max_Ambi_gl_s) :
    try :
      max_Ambi_gl = int(max_Ambi_gl_s)
    except :
      error(13, "-max_Ambi_global" + " (" + max_Ambi_gl_s + ")")
    if max_Ambi_gl < 0 :
      error(13, "-max_Ambi_global" + " (" + max_Ambi_gl_s + ")")

  # -max_Ambi_local n8
  if len(max_Ambi_loc_s) :
    try :
      max_Ambi_loc = int(max_Ambi_loc_s)
    except :
      error(13, "-max_Ambi_local" + " (" + max_Ambi_loc_s + ")")
    if max_Ambi_loc < 0 :
      error(13, "-max_Ambi_local" + " (" + max_Ambi_loc_s + ")")


  # Displaying an information during the execution
  # ==============================================

  # arguments seem right
  # if debug :
  message  = "Processing file " + fl_input_name + "\n"
  message += "Command:"
  for i in range(len(sys.argv)) :
    message += " " + sys.argv[i]
  print_msg(message)

  # Designing one or several triplets of primers for each snp
  # =========================================================
  message = "\nLaunching Primer3"
  print_msg(message)

  primers_triplets_design()

  # if debug :
  for i in range(len(snp_processing)) :
    if snp_processing[i] :
      count_SNP_processed += 1
    else :
      count_SNP_eliminated += 1

  for i in range(len(forward_nb_triplet)) :
    if forward_nb_triplet[i] or reverse_nb_triplet[i] :
      count_SNP_successful   += 1
      count_triplets_forward += forward_nb_triplet[i]
      count_triplets_reverse += reverse_nb_triplet[i]
  
  message  = "\n" + str(nb_snp) + " SNPs were submitted:\n"
  message += " - " + str(count_SNP_eliminated) + " were eliminated because they didn't respect quality constraints of flanking sequences.\n"
  message += " - " + str(count_SNP_processed) + " were submitted to Primer3.\n"
  message += " - at least one triplet was designed for " + str(count_SNP_successful) + " SNP giving a total of " + str(count_triplets_forward + count_triplets_reverse) + " triplets:\n"
  message += "   + " + str(count_triplets_forward) + " 'forward' triplets.\n"
  message += "   + " + str(count_triplets_reverse) + " 'reverse' triplets.\n"

  # testing the specificities of the triplets of primers
  # ====================================================
  if len(species_db_name) :

    print_msg("\nLaunching blastn ... Could be quite long ...")

    # amp_size_limit_1 : no amplification is expected if product size is beyond this limit
    # amp_size_limit_2 : amplification class : "+" if between amp_size_min and amp_size_max
    #                                          ""  if between 0 and amp_size_limit_2
    #                                          "-" if between amp_size_limit_2 and amp_size_limit_1
    amp_size_limit_1 = 1000
    if not amp_size_max :
      amp_size_max =  amp_size_max_dft
    if not amp_size_min :
      amp_size_min =  amp_size_min_dft
    if not amp_size_opt :
      amp_size_opt =  amp_size_opt_dft
    amp_size_limit_2 = amp_size_max + 4 * (amp_size_max - amp_size_opt)
    if amp_size_limit_2 > amp_size_limit_1 / 2 :
      amp_size_limit_2 = amp_size_limit_1 / 2

    for i in range(nb_snp) :
      # initialization of forward_nb_triplet_amp and reverse_nb_triplet_amp (count of triplets with expected amplification)
      # lists updated in  primers_triplet_specificity(i ,j ,direction)
      forward_nb_triplet_amp.append(0)
      reverse_nb_triplet_amp.append(0)

    # can be rather long ...
    testing_primers_triplets_specificities()

  # writing files including all the triplets of primers
  # ===================================================
  writing_all_primers_triplets_files()

  if len(species_db_name) :
    count_SNP_successful   = 0
    count_triplets_forward = 0
    count_triplets_reverse = 0
    for i in range(len(forward_nb_triplet_amp)) :
      if forward_nb_triplet_amp[i] or reverse_nb_triplet_amp[i] :
        count_SNP_successful   += 1
        count_triplets_forward += forward_nb_triplet_amp[i]
        count_triplets_reverse += reverse_nb_triplet_amp[i]

    message += " - at least one triplet was expected to be amplified for " + str(count_SNP_successful) + " SNP giving a total of " + str(count_triplets_forward + count_triplets_reverse) + " triplets with expected amplification:\n"
    message += "   + " + str(count_triplets_forward) + " 'forward' triplets.\n"
    message += "   + " + str(count_triplets_reverse) + " 'reverse' triplets.\n"

  print_msg(message)

  # Closing files
  # =============
  files_close()

  sys.exit(0)

