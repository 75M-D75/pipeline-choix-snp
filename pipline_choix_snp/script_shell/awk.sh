distance=$1
input_file_vcf=$2
output_file_vcf=$3
output_file_bed=$4


awk '{print($1,$2,$3,$4)}' $input_file_vcf | sed -e 's/ /\t/g' > $output_file_vcf
awk -v s1=$distance '{if ($2-s1 > 0) print($1,($2-s1),($2+s1)); }' $input_file_vcf | sed -e 's/ /\t/g' > $output_file_bed
