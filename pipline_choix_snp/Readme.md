# Pipeline choix SNP

*******
Contenus 
 1. [Configuration du fichier de paramettre ](#config_file)
 2. [Construction du fichier de configuration](#build_file)
 3. [Lancement du pipeline](#start_pipeline)
 4. [Deroulement du pipeline](#deroulement)
 5. [Résumer des fichiers de sortie](#resumer_out_files)

*******


## Décompression des données brutes (fichiers fastq) et réference (fichier format fasta) 

Créer un dossier où seront stockés les fichiers **fastq** (laisser une version non modifiée dans un dossier à part). Les fichiers fastq sont souvent sous forme compressés (extension .gz , .tar , .bz2 , …). Il y a au minimum 2 fichiers fastq pour les séquençages paired end, l’un pour le séquençage dans un sens l’autre pour l’autre sens.
Utiliser la bonne formule pour la décompression des données (Pour un fichier .gz par exemple, utiliser gzip -d fichier.fastq .gz )

Faire de même pour le fichier de référence (fichier **.fasta**), crée un dossier ou il sera stocké (de préférence pas le même que celui ou se trouve les **fastq**)

*******

<div id='config_file'/> 

## Configuration du fichier de paramettre 

Toutes les opérations suivantes qui seront exécutées dans le terminal doivent être effectué dans le répertoire **"pipepline_choix_snp"** pour cela utiliser la commande **"cd"**

Exemple : `cd /chemin/vers/pipepline_choix_snp/`


Si le répertoire se situe dans **/home/rep_work_pipeline/pipepline_choix_snp** lancer la commande : 

```
cd /home/rep_work_pipeline/pipepline_choix_snp/
```

### Parametres principaux

modifier dans le fichier **"fichier_parametre.txt"** (ce fichier n'est qu'un modèle) qui se trouve dans le répertoire **"Parametres"** les paramètres principaux à savoir :

* Le chemin absolu du répertoire ou se trouve les séquences **.fastq** (**repertoir_sequences**)
* Le chemin absolu du répertoire ou se trouve la référence **(référence)**
* Les noms des fichiers fastq **(parent_A_fastq_r1, parent_A_fastq_r2, parent_B_fastq_r1, parent_B_fastq_r2)**

***Note : Le fichier fichier_parametre.txt n'est qu'un modèle, vous pouvez crée votre propre fichier paramètre (choisir n'importe quel nom de fichier) à condition qu'il respecte le modèle c'est-à-dire que vous avez les mêmes noms de variable à l'intérieur suivi d'un signe "=" puis de ça valeur***

* Crée son propre fichier de paramètres permet d'éviter que deux utilisateurs travaille sur le même fichier de paramètre.
* Si vous crée votre propre fichier il est préférable de le stocker dans le répertoire **Parametres**

***Exemple : reference = /chemin/vers/reference.fasta***

#### Exemple 

Si vous avez mis les 4 fichiers fastq dans le répertoire suivant **"/home/rep_pipline_work/Bakour_Bergeron"** il vous faudra mettre comme valeur à la variable **"repertoir_sequences"** votre chemin : 

<div class="text_bloc" >

```
repertoir_sequences = /home/rep_pipline_peche_work/Bakour_Bergeron
```

</div>

vous devez ensuite renseigner les noms des fichiers fastq sans ajouté l'extension .fastq. par exemple si votre fichier **fastq** se nomme **"Bakour_vs_Ppv2ext.1.fastq"** alors il faut mettre **Bakour_vs_Ppv2ext.1** sans l'extension **.fastq** 

<div class="text_bloc" >

```
parent_A_fastq_r1   = Bakour_vs_Ppv2ext.1 

parent_A_fastq_r2   = Bakour_vs_Ppv2ext.2

parent_B_fastq_r1   = Bergeron_vs_Ppv2ext.1

parent_B_fastq_r2   = Bergeron_vs_Ppv2ext.2
```

</div>

Ensuite, il vous faudra spécifier le chemin absolu de la référence (fichier **fasta**)

#### Exemple

<div class="text_bloc" >

```
reference           = /home/reference/Marouch_Full_Genome_v2.0.fasta
```

</div>

Après cela vous aurez ainsi terminé la configuration des paramètres obligatoire, pour le reste référez-vous aux commentaires se situant dans le fichier **"fichier_parametre.txt"**.

:no_entry: ***N'oublier pas de sauvegarder les modifications.***

### Autre parametre

Vous pouvez modifier le **"parameter_file.txt"** qui sera utiliser pour les dessin d'amorce.

<div class="text_bloc" >

```
PRIMER_THERMODYNAMIC_OLIGO_ALIGNMENT=1
PRIMER_MAX_TM=72
PRIMER_MIN_TM=55
PRIMER_OPT_TM=60
PRIMER_PRODUCT_SIZE_RANGE=45-100
PRIMER_PRODUCT_OPT_SIZE=45
PRIMER_MAX_SIZE=32
PRIMER_MIN_SIZE=18
PRIMER_OPT_SIZE=23
PRIMER_PAIR_MAX_DIFF_TM=1
PRIMER_MAX_SELF_END=2
PRIMER_MAX_SELF_ANY=6
PRIMER_NUM_RETURN=10
```

</div>

*******

<div id='build_file'/>

### Construction du fichier de configuration

Après avoir entrée vos paramètres dans le fichier **"fichier_parametre.txt"** vous devrez lancer le script **"parse_params.py"** se situant dans le dossier **script_python** 

si vous êtes dans le terminal et que votre chemin courant est celui où se trouve le fichier **"pipeline.snk"** c'est-à-dire le répertoire **"pipepline_choix_snp"**.

Exemple : `python script_python/parse_params.py Parametres/nom_du_fichier_parametre.txt Config/nom_du_fichier_config.json`


##### Exemple

```
python script_python/parse_params.py Parametres/fichier_parametre.txt Config/config.json
```

Cette commande va crée un fichier de configuration **"config.json"** contenant les paramètres que vous aurez choisis dans le répertoire **"Config"**

***Note : Vous pouvez nommer votre fichier de configuration comme vous le souhaitez à condition qu'il se termine par l'extension .json***

Exmple : `python script_python/parse_params.py Parametres/fichier_parametre.txt Config/bakour_bergeron.json`

* Crée son propre fichier de configuration permet d'éviter que deux utilisateurs travaille sur le même fichier de configuration.
* Si vous crée votre propre fichier il est préférable de le stocker dans le répertoire **Parametres**


*******

<div id='start_pipeline'/>

### Lancement du pipeline

```
snakemake --snakefile pipeline.snk --configfile Config/config.json
```

Si vous souhaitez que le pipeline se termine à une certaine étape, il suffit pour cela de le lancer en spécifiant en derniers arguments sur quelle étape doit se terminer le processus.

Exemple si vous souhaitez vous arrêtez à l'étape du choix des snp qui est nommé dans le pipeline par **"choix_snp"** il suffit de lancer : 

```
snakemake --snakefile pipeline.snk --configfile Config/config.json choix_snp
```

##### Shéma des différentes étapes (régles)

<img src="dag.svg">

Enjoy :+1:

<dir id="deroulement"></dir>

## Deroulement du pipeline

Le pipeline contient 10 règles chaque règle est composée d'une ou plusieurs tâches qui au total on pour but d'effectuer 8 étapes.

##### Etape 1 : Nettoyage, Filtrage ...

La première étape est le nettoyage, filtrage, analyse de qualité des reads, et test de présence d’adaptateurs sur les fichiers **fastq**. Cette tâche est effectuée avec le programme **fastp**. 

:outbox_tray: 

En sortie cette étape crée un dossier **"Etape1_Fastp"** qui contient les séquences nettoyées et un dossier **"Fastp_report"** qui contient un rapport détailler des résultats obtenus pour chaque ***parent*** sous forme de fichier **html et json**. (régle fatsp)

***Nom régle : fatsp***

##### Etape 2 : Alignement (mapping)

La seconde étape est l'alignement effecteur avec **bowtie2** ainsi que l'indexation de la référence. 

:outbox_tray:

En sortie cette étape crée un dossier **Index** qui contient tous les fichiers d'indexage. Ces fichiers sont longs à générer il est nécessaire d'éviter de les supprimer lorsque l'on souhaite relancer le pipeline. (régle bowtie)


***Nom régle : bowtie***

##### Etape 3 : SNP Calling

Cette étape correspond au SNP Calling, elle est effectuée avec **samtools** et **Varscan** elle permet de trouver les **SNP** et les **INDEL**.

Elle prend en compte les différents paramètres entrés : 
 

<div class="text_bloc" >

```
min_coverage    : correspond à la profondeur à partir de laquelle le logiciel va rechercher un SNP 
min_reads       : minimum de reads à la position du snp potentiel pour que le logiciel accepte le SNP
min_avg_qual    : qualité de la base minimum pour que le logiciel considère que c’est un read et l’inclus dans l’analyse.
min_var_freq    : fréquence minimum du SNP pour que le logiciel le detecte.
```

</div>

:outbox_tray:

En sortie cette etape va crée un dossier **Etape3_SNP_Calling** qui contient 2 fichiers **.vcf** et un fichier **.mpileup** le premier vcf **Variants_cns_0.3.vcf** (0.3 correspond à la fréquence minimal choisi par l'utilisateur) correspond à l'ensemble des SNP et INDEL, le second vcf **SNP_Only.vcf** contient uniquement les SNP. (régles sam_bam, bam_mpileup, recherche_snp_indel)

***Nom régle : recherche_snp_indel***

##### Etape 4 : Choix des SNP en fonction du type F1 ou F2

Cette étape va extraire les SNP en fonction de certains critères pour la F1 et F2. Cette tache est effectuée par le script **script_python/choix_snp.py**

**Critère F1 :** 

<div class="text_bloc" >

```
Parent A   Parent B
  A/C        C/C
  A/A        A/C
  A/C        A/C
```

</div>

**Critère F2 :**

<div class="text_bloc" >

```
Parent A   Parent B
  A/A        B/B
  A/A        A/C
  A/C        C/C
```

</div>

La détection des SNP hétérozygote ou homozygote est effectrice selon le **seuil** donné en paramètre. 

Exemple prenons un seuil de 0.4 


```
seuil = 0.4
borne_homo_inferieur   = 0.5 - seuil = 0.1
borne_hetero_inferieur = 0.5 - borne_homo_inferieur = 0.4
borne_hetero_superieur = 0.5 + borne_homo_inferieur = 0.6
borne_homo_superieur   = 1 - borne_homo_inferieur = 0.9
```

le SNP est donc **hétérozygote** si sont poucentage est compris entre :
```
borne_hetero_inferieur <= heterozygote <= borne_hetero_superieur
0.4 <= heterozygote <= 0.6
```

Il est **homozygote** si :
```
homozygote > borne_homo_superieur
homozygote > 0.9

homozygote < borne_homo_inferieur
homozygote < 0.1
```

Le reste sont des zones d'incertitude
```
borne_homo_inferieur < zone_incertitude < borne_hetero_inferieur
0.1 < zone_incertitude < 0.4

borne_hetero_superieur < zone_incertitude < borne_homo_superieur
0.6 < zone_incertitude < 0.9
```


```
seuil               : config["seuil"],
min_coverage_snp    : correspond à la profondeur minimum à laquelle un SNP sera accepter dans les résultat F1 ou F2.
max_coverage_snp    : correspond à la profondeur maximum à laquelle un SNP sera accepter dans les résultat F1 ou F2.
```

:outbox_tray:

En sortie cette étape crée un dossier **"Etape4_SNP_F1_F2"** qui contient trois fichiers **SNPselectionner_F1.vcf**, **SNPselectionner_F2.vcf** et **SNPselectionner_F2_optimum.vcf** qui contiennent les SNP extraits selon les critères précédents, **SNPselectionner_F2_optimum.vcf** ne contient que les SNP homozygotes différents. (règle choix_snp)

***Nom régle : choix_snp***

##### Etape 5 : Dessin des amorces (Kaspar)

Cette étape dessine les amorces en fonction des SNP trouvés d'abord un fichier **.bed** est construit avec le script **"script_shell/awk.sh"** puis un fichier fasta est généré avec l'image du container **"Simg/bedtools.simg"** qui permet de générer un autre fichier au format **fasta** contenant les **SNP** ceci est générer par le script **"script_python/build.py"** et c'est donc avec ce fichier générer que **Simg/kaspar.img** sera lancé afin de dessiner les amorces.

:outbox_tray:

En sortie cette étape crée un dossier **"Etape5_Kasp"** qui contient **"all_primers_triplets.csv"** un fichier de type tableur contenant toutes les amorces dessiner. (régles kaspar)

***Nom régle : kaspar***

##### Etape 6 : Suppression des primers avec variants proche

Cette étape consiste à supprimer tous les primers ou amorces qui contiennent des variants à l'intérieur. Elle est effectuée par le script **"suppression_primers_sans_proche.py"** se trouvant dans le dossier **"script_python"** 

:outbox_tray:

En sortie cette étape crée un dossier **"Etape6_Primers_sans_variants"** qui contient **"sans_proche_primers_triplets.csv"** un fichier de type tableur ne contenant que les primers sans variants à l'intérieur. (règles suppression_primers_sans_variants_proche )

***Nom régle : suppression_primers_sans_variants_proche***

##### Etape 7 : Meilleur Primers

Cette étape consiste à sélectionner les primers ayant le meilleur score, parmi les SNP conservés pour la **F1** ou **F2**. Elle est effectuer par le script **"filtre_primer.py"** se trouvant dans le dossier **"script_python"**.

:outbox_tray:

En sortie cette étape crée un dossier **"Etape7_Best_Primers"** qui contient **"all_best_primers_triplets_F1.csv"** ou **"all_best_primers_triplets_F2.csv"** en fonction du choix F1 ou F2 (paramètre **type_F**). (règles best_primers)

***Nom régle : best_primers***

##### Etape 8 : Repartition

Cette étape consiste à repartir les meilleures primers sans variants, sur le génome en fonction de la taille de la puce (paramètre **taille_puce**) 

:outbox_tray:

En sortie cette étape crée un dossier **"Etape8_Repartition"** qui contient **"rep_best_primers_triplets_F1.csv"** ou **"rep_best_primers_triplets_F2.csv"** selon le type F choisi (paramètre **type_F**) ce fichier contient les primers qui ont pu être repartie sur le génome.  

***Nom régle : repartition***


*******

<div id="resumer_out_files"></div>

### Résumer des fichiers de sortie :open_file_folder:

Exemple de fichier de sortie obtenu aprés utilisation du pipeline.

Etape 1
```
Etape1_Fastp/
├── Bakour_vs_Ppv2ext.1.out_fastp.fastq
├── Bakour_vs_Ppv2ext.2.out_fastp.fastq
├── Bergeron_vs_Ppv2ext.1.out_fastp.fastq
├── Bergeron_vs_Ppv2ext.2.out_fastp.fastq
└── Fastp_report
    ├── parent_A_fastp.html
    ├── parent_A_fastp.json
    ├── parent_B_fastp.html
    └── parent_B_fastp.json

1 directory, 8 files
```

Etape 2
```
./Index/
├── bt2.1.bt2
├── bt2.2.bt2
├── bt2.3.bt2
├── bt2.4.bt2
├── bt2.rev.1.bt2
└── bt2.rev.2.bt2

0 directories, 6 files
```


Etape 3
```
./Etape3_SNP_Calling/
├── SNP_Only.vcf
├── Variants_cns_0.3.vcf
└── Variants_cns.sorted.mpileup

0 directories, 3 files
```

Etape 4
```
Etape4_SNP_F1_F2/
├── SNPselectionner_F1.vcf
├── SNPselectionner_F2_optimum.vcf
└── SNPselectionner_F2.vcf

0 directories, 3 files
```

Etape 5
```
./Etape5_Kasp/
├── all_primers_triplets.csv
├── nb_triplets_by_snp.csv
└── primers_triplets_specificities.csv

0 directories, 3 files
```

Etape 6
```
./Etape6_Primers_sans_variants/
└── sans_proche_primers_triplets.csv

0 directories, 1 file
```

Etape 7
```
./Etape7_Best_Primers/
└── all_best_primers_triplets_F2.csv

0 directories, 1 file
```

Etape 8
```
./Etape8_Repartition/
└── rep_best_primers_triplets_F2.csv

0 directories, 1 file
```

***Un fichier Bilan.txt est générer en plus, ainsi que des fichier .jpeg contenant la densité des SNP***

### Annexe 

#### Fichier modele de parametre

***fichier_parametre.txt***

<div class="text_bloc" >

```

/**** Chemin du repertoir ou se trouve les fichiers ".fastq" des deux parents ****/

repertoir_sequences = /home/mmohamed/Documents/Donnee_serveur/bergeron_et_bakour


/**** Nom du fichier ".fastq" du parent A des premiers reads
exemple si votre fichier fastq se nomme "Bakour_vs_Ppv2ext.1.fastq" alors il faut mettre Bakour_vs_Ppv2ext.1 sans l'extension .fastq ****/

parent_A_fastq_r1   = Bakour_vs_Ppv2ext.1

/**** Nom du fichier ".fastq" du parent A des seconds reads 
 exemple si votre fichier fastq se nomme "Bakour_vs_Ppv2ext.1.fastq" alors il faut mettre Bakour_vs_Ppv2ext.1 sans l'extension .fastq ****/

parent_A_fastq_r2   = Bakour_vs_Ppv2ext.2

/**** Nom du fichier ".fastq" du parent B des premiers reads
 exemple si votre fichier fastq se nomme "Bakour_vs_Ppv2ext.1.fastq" alors il faut mettre Bakour_vs_Ppv2ext.1 sans l'extension .fastq ****/

parent_B_fastq_r1   = Bergeron_vs_Ppv2ext.1

/**** Nom du fichier ".fastq" du parent B des seconds reads
 exemple si votre fichier .fastq se nomme "Bakour_vs_Ppv2ext.1.fastq" alors il faut mettre Bakour_vs_Ppv2ext.1 sans l'extension .fastq ****/

parent_B_fastq_r2   = Bergeron_vs_Ppv2ext.2

/**** Chemin ou se trouve la reference au format fasta ****/

reference           = /home/mmohamed/Documents/Donnee_serveur/reference/Marouch_Full_Genome_v2.0.fasta 


/**** Taille de la puce utiliser ****/

taille_puce         = 288

/**** correspond à la profondeur à partir de laquelle le logiciel va rechercher un SNP. Cela dépend de la couverture de la séquence, si on met 4 et que la séquence à une couverture moyenne de 3, ça n’analysera pas grand-chose ! ****/

min_coverage        = 14

/**** correspond à la profondeur minimum à laquelle un SNP sera accepter dans les résultat F1 ou F2 ****/

min_coverage_snp    = 4


/**** correspond à la profondeur maximum à laquelle un SNP sera accepter dans les résultat F1 ou F2 ****/

max_coverage_snp    = 45

/**** minimum de reads à la position du snp potentiel pour que le logiciel accepte le SNP ****/

min_reads           = 14

/**** qualité de la base minimum pour que le logiciel considère que c’est un read et l’inclus dans l’analyse. ****/

min_avg_qual        = 30

/**** fréquence minimum du SNP pour que le logiciel le detecte. ****/

min_var_freq        = 0.3

/**** Nombre de nucleotide max avant et aprés les SNP pour les amorces la valeur doit être supérieur ou egal à 51 ****/

distance            = 70

/**** Choisir si les fichiers .bam, .bai, .idxstats doivent être supprimer, deux valeur sont possible : true, false ****/

suppression_bam     = true

/**** seuil permetant d'identifier les SNP heterozyge et homozygote (voir le details des calcul dans la doc) ****/

seuil               = 0.4


/**** Indiquer si c'est une F1 ou F2  ****/

type_F              = F2



```

</div>