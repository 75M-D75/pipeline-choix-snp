import sys
import re
import os
import json

import argparse


parser = argparse.ArgumentParser(description="read a file containing the parameters and build a parameter file (json) suitable for the pipeline")

#MAIN ARGS
parser.add_argument("param_file", type=str,
                    help="parameter file")
parser.add_argument("output_file", type=str,
                    help="name of json output file")

args = parser.parse_args()

name_file 		= args.param_file
name_file_out	= args.output_file

fichier_parmetre	= open(name_file, "r")
fichier_json		= open(name_file_out, "w")
lignes				= fichier_parmetre.readlines()

dico 				= {
	"repertoir_sequences":"/path/to/seq/",
    "parent_A_fastq_r1":"Bakour_vs_Ppv2ext.1",
    "parent_A_fastq_r2":"Bakour_vs_Ppv2ext.2",
    "parent_B_fastq_r1":"Bergeron_vs_Ppv2ext.1",
    "parent_B_fastq_r2":"Bergeron_vs_Ppv2ext.2",
    "reference":"/path/to/genome/genome.fasta",
    "Bam":"Bam_parent",
    "suppression_bam":"true",
    "taille_puce":3000,
	"min_coverage":4,
    "min_reads":4,
    "min_avg_qual":30,
    "min_var_freq":0.3,
    "min_coverage":14,
	"type_F":"F2",
    "distance":51
}

expression			= "([^\s]*)[\s]*=[\s]*(.*)"

for line in lignes :
	x = re.search(expression, line)
	if x :
		name_variable 		= x.group(1)
		value 				= x.group(2)

		x_x					= re.search("([0-9.,]+$)", value)

		#/path/to/seq/ --> /path/to/seq
		value.strip("/")

		value.replace(" ", "")
		# si c'est un nombre
		if x_x and len(x_x.group(1)) == len(value) :

			value = value.replace(",", ".")

			if re.search("\.", value) :
				dico[name_variable] = float(value)
			else :
				dico[name_variable] = int(value)

		else :
			dico[name_variable] = value

		if name_variable == "reference":
			name_rep_ref = "/".join( value.split("/")[:-1] )
			dico["rep_ref"] = name_rep_ref


fichier_json.write( json.dumps(dico, indent=4) )

fichier_json.close()
fichier_parmetre.close()