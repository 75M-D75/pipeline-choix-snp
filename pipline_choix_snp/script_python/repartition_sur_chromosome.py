import sys
import re
import os
import pandas as pd


def help():
	print("help")

name_file_csv    = "all_best_primers_triplets_F2.csv"
directory_out	 = ""
type_F           = "F2"

i = 1
nb_arg = len(sys.argv)
while i < nb_arg :
    if sys.argv[i] == "-h" :
        help()
    elif sys.argv[i] == "-csv" and i < nb_arg :
        name_file_csv   = sys.argv[i + 1]
        i += 1
    elif sys.argv[i] == "-size" and i < nb_arg :
        size_genome     = int(sys.argv[i + 1].split("/")[0])
        size_puce       = int(sys.argv[i + 1].split("/")[1])
        i += 1
    elif sys.argv[i] == "-dout" and i < nb_arg :
        directory_out   = sys.argv[i + 1]
        i += 1
    elif sys.argv[i] == "-type" and i < nb_arg :
        type_F          = sys.argv[i + 1]
        i += 1
    i += 1


distance 				= int(size_genome / size_puce)
interval				= int(distance / 2)
i 						= 0
expression_posistion 	= "^[^\s]+\s([0-9]+)"
compteur				= 0
chrom_pred = ""

print("distance de repartition : ", distance)

df 	= pd.read_csv(name_file_csv, sep=";")
nb_line = len(df.values)
def repartition(df):
	tab_to_graph 	= []
	tab_to_csv_rep  = []
	compteur 		= 0
	chrom_pred 		= df.iloc[0,0].split("_pos")[0]

	for indice, line in enumerate(df.values):
		if chrom_pred != df.iloc[indice,0].split("_pos")[0] :
			compteur 	= 0
			chrom_pred 	= df.iloc[indice,0].split("_pos")[0]

		position = int(df.iloc[indice,0].split("_pos")[1])
		while position > (distance * compteur) + interval :
			compteur += 1

		pos_proche = 0
		while indice + pos_proche < nb_line - 1 and chrom_pred == df.iloc[indice + pos_proche,0].split("_pos")[0] and int(df.iloc[indice + pos_proche,0].split("_pos")[1])  < (distance * compteur) :
			pos_proche += 1

		if pos_proche - 1 > 0:
			pos_proche -= 1
			position = int(df.iloc[indice + pos_proche, 0].split("_pos")[1])

		if position >= (distance * compteur) - interval and position <= (distance * compteur) + interval :
			#print("trouver", position, ((distance * compteur) - interval), ((distance * compteur) + interval))
			#SNP, chromosome, position
			#chr1_pos1999, chr1, 1999
			tab_to_csv_rep.append(df.iloc[indice + pos_proche])
			tab_to_graph.append([ df.iloc[indice + pos_proche,0], df.iloc[indice + pos_proche,0].split("_pos")[0], df.iloc[indice + pos_proche,0].split("_pos")[1] ])
			#tab_to_graph.append([ df.iloc[indice,0], df.iloc[indice,0].split("_pos")[0], (distance * compteur) ])

			compteur += 1

	df_out 		= pd.DataFrame(tab_to_csv_rep, columns = df.columns)
	df_out_rep 	= pd.DataFrame(tab_to_graph, columns = ['SNP', 'Chromosome', 'Position'])

	nb_amorce_reparti = len(tab_to_graph)
	return (df_out, df_out_rep, nb_amorce_reparti )


if directory_out != "" and directory_out[-1] != "/" :
	directory_out += "/"


df_out, df_out_rep, nb_amorce_reparti = repartition(df)
df_out.to_csv(directory_out+"rep_best_primers_triplets_"+str(type_F)+".csv", sep=";", index=False)
df_out_rep.to_csv(directory_out+"repartition_"+str(type_F)+".csv", sep=";", index=False)
print("nb amorces repartie "+str(type_F)+" : ", nb_amorce_reparti)