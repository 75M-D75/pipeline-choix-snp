import sys
import re
import os
import pandas as pd
import argparse

parser = argparse.ArgumentParser()

#require
parser.add_argument("-csv", type=str,
                    help="VCF file ")
parser.add_argument("-vcf", type=str,
                    help="vcf file containing only SNP")
args = parser.parse_args()


#par defaut
name_file_csv = args.csv
name_file_vcf = args.vcf

i 						= 0
expression_posistion 	= "^[^\s]+\s([0-9]+)"
chrom_pred = ""

df 	= pd.read_csv(name_file_vcf, sep="\t", header=None)

def repartition(df):
	tab_to_graph 	= []
	chrom_pred 		= df.iloc[0,0]

	for indice, line in enumerate(df.values):
		
		tab_to_graph.append([ (str(df.iloc[indice,0]) + "_pos" + str(df.iloc[indice,1])), df.iloc[indice,0], int(df.iloc[indice,1]) ])
			
	df_out 	= pd.DataFrame(tab_to_graph, columns = ['SNP', 'Chromosome', 'Position'])

	nb_snp_reparti = len(tab_to_graph)
	return (df_out, nb_snp_reparti )

################## VCF ###############

df_out, nb_snp_reparti = repartition(df)
df_out.to_csv(name_file_csv, sep=";", index=False)
print("nb snp repartie : ", nb_snp_reparti)

