import sys
import re
import os
import argparse

parser = argparse.ArgumentParser("Builds a file containing a list of SNPs with their mandatory forward and backward sequences (for kaspar)")

#require
parser.add_argument("distance", type=int,
                    help="SNP distance")
parser.add_argument("vcf", type=str,
                    help="vcf file")
parser.add_argument("fasta", type=str,
                    help="fasta file")
parser.add_argument("out", type=str,
                    help="output file containing a list of SNPs with their mandatory forward and backward sequences")

args = parser.parse_args()


distance = args.distance

name_file_entree_1  = args.vcf
name_file_entree_2  = args.fasta
name_file_sorite    = args.out

#name_file_entree_1     = "new.vcf"
#name_file_entree_2     = "file_fasta.fasta"
#name_file_sorite   = "new_fasta.txt"

fichier_entree_1        = open(name_file_entree_1, "r")
fichier_entree_2        = open(name_file_entree_2, "r")
fichier_sortie          = open(name_file_sorite  , "w")

expression              = "(.*)\s([0-9]+)\s([A-Z])\s([A-Z])$"

ligne_file_2            = fichier_entree_2.readline()
ligne_file_1            = fichier_entree_1.readline()
ligne_file_2            = fichier_entree_2.readline()

i = 0
while ligne_file_1 and ligne_file_2  :

    x = re.search(expression, ligne_file_1)
    line_insert = ""

    if x :
        chromosome  = x.group(1)
        position    = x.group(2)
        reference   = x.group(3)
        variant     = x.group(4)
        line_insert = ligne_file_2[:(int(distance)-1)] + "[" + reference + "/" + variant + "]" + ligne_file_2[int(distance):]
    
    ligne_file_1 = fichier_entree_1.readline()
    ligne_file_2 = fichier_entree_2.readline()

    fichier_sortie.write(">" + chromosome + "_pos" + position + "\n")
    fichier_sortie.write(line_insert)

    while ligne_file_2 and re.search("^>", ligne_file_2) :
        ligne_file_2 = fichier_entree_2.readline()

    i += 1

fichier_entree_1.close()
fichier_entree_2.close()
fichier_sortie.close()