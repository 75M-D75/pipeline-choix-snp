import sys
import re
import os

name_file_in 	= ""
min_coverage 	= 4
max_coverage 	= 20
seuil 			= 0.4

name_file_out_F1 = "SNPselectionner_F1.vcf"
name_file_out_F2 = "SNPselectionner_F2.vcf"

def help():
	print("help")

#print(sys.argv)

i = 1
nb_arg = len(sys.argv)
while i < nb_arg :
	if sys.argv[i] == "-h" :
		help()
	elif sys.argv[i] == "-in" and i < nb_arg :
		name_file_in = sys.argv[i + 1]
		i += 1
	elif sys.argv[i] == "-c" and i < nb_arg :
		min_coverage = int(sys.argv[i + 1].split("/")[0])
		max_coverage = int(sys.argv[i + 1].split("/")[1])
		i += 1
	elif sys.argv[i] == "-s" and i < nb_arg :
		seuil = round(float(sys.argv[i + 1]), 2)
		i += 1
	elif sys.argv[i] == "-F1" and i < nb_arg :
		name_file_out_F1 = sys.argv[i + 1]
		i += 1
	elif sys.argv[i] == "-F2" and i < nb_arg :
		name_file_out_F2 = sys.argv[i + 1]
		i += 1
	i += 1

s2 	= 0.5 - seuil
s3 	= 0.5 - s2
s4 	= 0.5 + s2 

expression_plus = "[\+\-][A-Z]"
expression_vir  = "[A-Z]\s([A-Z],)"

fichier_entree 	= open(name_file_in, "r")
fichier_sortie1 = open(name_file_out_F1, "w")
fichier_sortie2 = open(name_file_out_F2, "w")
fichier_sortie3 = open(name_file_out_F2[:-4]+str("_optimum.vcf"), "w")

def zone_incertitude(percent):
	return (percent > s2 * 100 and percent < s3 * 100) or (percent > s4 * 100 and percent < (1-s2) * 100)

def heterozygote_percent(percent):
	global s3
	global s4
	return (s3 * 100 <= percent and percent <= s4 * 100)

expression_het_hom = ".*\s[A-Z]\s[A-Z]\s.*\s[0-9]\s([A-Z]).*\s([A-Z])"

nb_heterozygote = [0] * 2 #nombre hetero pour chaque parent
compt = 0
ligne = fichier_entree.readline()
while ligne :
	x_het_hom	 	= re.search(expression_het_hom, ligne)
	split_line 		= ligne.split("\t")
	freq_read1 		= 0
	freq_read2 		= 0
	cov_read1 		= 0
	cov_read2 		= 0
	
	regex_freq_read1 	= re.search("([0-9,]+)%", split_line[10].split(":")[0])
	regex_freq_read2 	= re.search("([0-9,]+)%", split_line[10].split(":")[1])
	regex_cov_read1		= re.search("[A-Z]:([0-9]+)", split_line[10].split(":")[0])
	regex_cov_read2		= re.search("[A-Z]:([0-9]+)", split_line[10].split(":")[1])
	if regex_freq_read1 and regex_freq_read2 :
		freq_read1 = round(float(regex_freq_read1.group(1).replace(",",".")), 2)
		freq_read2 = round(float(regex_freq_read2.group(1).replace(",",".")), 2)

		if x_het_hom :

			cov_read1  = int(regex_cov_read1.group(1))
			cov_read2  = int(regex_cov_read2.group(1))

			if not zone_incertitude(freq_read1) and not zone_incertitude(freq_read2) and min_coverage <= cov_read1 and cov_read1 <= max_coverage and min_coverage <= cov_read2 and cov_read2 <= max_coverage :
				#F1 A/C   C/C
				if  heterozygote_percent(freq_read1) and not heterozygote_percent(freq_read2) :
					fichier_sortie1.write(ligne)
					fichier_sortie2.write(ligne)
					nb_heterozygote[0] += 1
				#F1 A/A   A/C
				elif not heterozygote_percent(freq_read1) and heterozygote_percent(freq_read2) :
					fichier_sortie1.write(ligne)
					fichier_sortie2.write(ligne)
					nb_heterozygote[1] += 1
				#F2 A/A   B/B
				elif not heterozygote_percent(freq_read1) and not heterozygote_percent(freq_read2) and ( (freq_read1 <= s4 * 100 or freq_read2 >= ((1-s2) * 100)) or (freq_read2 <= s4 * 100 or freq_read1 > ((1-s2) * 100)) ):
					fichier_sortie3.write(ligne)
					fichier_sortie2.write(ligne)
					
			else :
				compt += 1

	ligne = fichier_entree.readline()

print("nombre heterozygote parent A : "+str(nb_heterozygote[0]))
print("nombre heterozygote parent B : "+str(nb_heterozygote[1]))
#print("nb A/A B/B ; A/B B/C", compt)
fichier_entree.close()
fichier_sortie1.close()
fichier_sortie2.close()
fichier_sortie3.close()