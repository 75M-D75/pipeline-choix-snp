import sys
import re
import os
import pandas as pd
parser = argparse.ArgumentParser("Builds a file containing a list of SNPs with their mandatory forward and backward sequences (for kaspar)")

#require
parser.add_argument("-i", type=str,
                    help="SNP distance")
parser.add_argument("vcf", type=str,
                    help="vcf file")
parser.add_argument("-ocsv", type=str,
                    help="fasta file")
parser.add_argument("-ivcf", type=str,
                    help="output file containing a list of SNPs with their mandatory forward and backward sequences")

parser.add_argument("-s", "--seuil", type=str,
                    help="output file containing a list of SNPs with their mandatory forward and backward sequences")


args = parser.parse_args()


csv_file_in   = args.i
name_file_out = args.ocsv
name_file_in  = args.ivcf

if args.seuil:
  seuil = round(float(args.seuil), 2)

df = pd.read_csv(csv_file_in, sep=";")

max_val           = 0
chrom_max_score   = []
chrom_pred        = df.iloc[0,0]
indice_max_val    = 0


for indice, line in enumerate(df.values):

    #reinitialiser au chromosome suivant
    if df.iloc[indice,0] != chrom_pred :
        chrom_max_score.append(indice_max_val)
        indice_max_val  = 0
        chrom_pred      = df.iloc[indice,0]
        max_val         = 0
        
    if max_val < line[26]:
        max_val         = line[26]
        indice_max_val  = indice

chrom_max_score.append(indice_max_val)

fichier_entree = None

try:
  fichier_entree = open(name_file_in, "r")
except:
  print("Erreur : open("+name_file_in+", r)")
  exit(-1)

ligne   = fichier_entree.readline()

indice  = 0
tab     = []
while ligne :
  split_line = ligne.split("\t")
  if ((split_line[0] + "_pos" + split_line[1]) in df["id"].values) and len(df.iloc[chrom_max_score][ df["id"] == (split_line[0] + "_pos" + split_line[1]) ]) > 0:
    tab.append(df.iloc[chrom_max_score][ df["id"] == (split_line[0] + "_pos" + split_line[1]) ].iloc[0]) 

  ligne   = fichier_entree.readline()
  indice += 1

df_out = pd.DataFrame(tab, columns = df.iloc[chrom_max_score].columns)
df_out.to_csv(name_file_out, sep=";", index=False)

fichier_entree.close()

