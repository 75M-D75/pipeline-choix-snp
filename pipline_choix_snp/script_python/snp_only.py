import sys
import re
import os
import argparse

parser = argparse.ArgumentParser("Get SNP in VCF file")

#MAIN ARGS
parser.add_argument("-i", type=str,
                    help="VCF file")
parser.add_argument("-out", type=str,
                    help="file out VCF only SNP")

args = parser.parse_args()


name_file_in 	= args.i
name_file_out	= args.out

fichier_entree 	= open(name_file_in, "r")
fichier_sortie 	= open(name_file_out, "w")

expression_plus = "[\+\-][A-Z]"
expression_vir  = "[A-Z]\s([A-Z],)"

#
i 						= 0
compt_suppression 		= 0
compteu 				= [0, 0, 0]
ligne 					= fichier_entree.readline()
ligne 					= fichier_entree.readline()
while ligne :
	x_plus 		= re.search(expression_plus, ligne)
	x_virgule 	= re.search(expression_vir, ligne)
	
	if not x_plus and not x_virgule :
		compteu[0] += 1
	else :
		compteu[1] += 1

	if not x_plus and not x_virgule :
		fichier_sortie.write(ligne)

	ligne = fichier_entree.readline()

	i += 1

print('total variants : ', i-1)
print("nombre snp : ", compteu[0])
print("nombre INDEL : ", compteu[1])


fichier_entree.close()
fichier_sortie.close()