import sys
import re
import os
import pandas as pd
import math
import argparse


parser = argparse.ArgumentParser()

#require
parser.add_argument("-csv", type=str,
                    help="fichier primers csv ")

parser.add_argument("-vcf", type=str,
                    help="fichier vcf SNP")


#option
parser.add_argument("-out", type=str,
                    help="fichier de sortie tabulé", default="sans_proche_primers_triplets.csv")

parser.add_argument("-d", "--distance", type=int,
                    help="distance des SNP", default=70)

args = parser.parse_args()




name_file_csv = args.csv
name_file_vcf = args.vcf

name_file_out = args.out
distance      = args.distance


df      = pd.read_csv(name_file_csv, sep=";")
df_vcf  = pd.read_csv(name_file_vcf, sep="\t")


tab = []
for i, l in enumerate(df.values) :
    if not math.isnan(df.iloc[i]["start"]) :
        start = int(df.iloc[i]["start"])
        size  = int(df.iloc[i]["len"])
        chromosome          = l[0].split("_pos")[0]
        position_variant    = int(l[0].split("_pos")[1])
        position_deb_amorce = (position_variant - distance) + start
        position_fin_amorce = position_deb_amorce + size

        #print(l[0].split("_pos"), start, position_deb_amorce, position_fin_amorce)
        if len(df_vcf[ df_vcf["Chrom"] == chromosome ][ df_vcf["Position"] >= position_deb_amorce ][ df_vcf["Position"] <= position_fin_amorce ].values) == 0 :
            tab.append(df.iloc[i])
            
df_out = pd.DataFrame(tab, columns = df.columns)
df_out.to_csv(name_file_out, sep=";", index=False)

    


